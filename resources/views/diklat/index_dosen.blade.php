@extends('layouts.app')

@section('title')
Data Diklat {{ $dosen->nama }}
@endsection

@section('content')


<div class="row">
    <div class="col-md-12">
        @include('dosen.tabs')
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h4>Diklat {{ $dosen->namaGelar() }}</h4>
                </div>
                <div class="card-body">
                <a href="{{ route('diklat.create', $dosen->id) }}" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Tambah Diklat</a>
                    <table class="table diklat-table">
                        <tr>
                            <th>No</th>
                            <th>Nama Diklat</th>
                            <th>Penyelenggara</th>
                            <th>Tingkat</th>
                            <th>Tempat</th>
                            <th>Nomor Sertifikat</th>                           
                            <th>Tahun</th>
                            <th>Mulai</th>
                            <th>Selesai</th>
                            <th>Sertifikat</th>
                            <th>Aksi</th>
                        </tr>       
                       
                        @forelse($diklat as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->nama_diklat }}</td>
                            <td>{{ $row->penyelenggara }}</td>
                            <td>{{ $row->getTingkat() }}</td> 
                            <td>{{ $row->tempat }}</td> 
                            <td>{{ $row->nomor_sertifikat }}</td>
                            <td>{{ $row->tahun_penyelenggaraan }}</td>
                            <td>{{ $row->tanggal_mulai }}</td>
                            <td>{{ $row->tanggal_selesai }}</td>
                            <td>
                                @if($row->file_sertifikat != null || $row->file_sertifikat != '')
                                <a href="{{ asset('storage/diklat/'. $row->file_sertifikat) }}">Download</a>
                                @else 
                                null
                                @endif
                            </td>
                            <td>
                                <form action="{{ route('diklat.destroy', $row->id) }}" method="post">
                                    <ul class="d-flex action-button">
                                        @csrf
                                        @method('delete')
                                        <li><a href="{{ route('diklat.edit', $row->id) }}" class="text-secondary" title="Edit"><i class="fa fa-edit"></i></a></li>
                                        <!-- <li><button type="submit" class="text-danger btn-submit" onclick="return confirm('Yakin akan hapus data?')" ><i class="fa fa-trash"></i></button></li> -->
                                    </ul>
                                </form>
                            </td>
                        </tr>
                        @empty 
                        <tr>
                            <td colspan="11">Dosen ini belum memiliki diklat.</td>
                        </tr>
                        @endforelse
                        
                        
                    </table>
                </div>
            </div>
    </div>
</div>



@endsection