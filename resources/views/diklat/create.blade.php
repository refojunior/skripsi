@extends('layouts.app')

@section('title')
Tambah Diklat 
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Tambah Diklat</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('diklat.store') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                    @csrf
                    
                    <div class="form-group row">       
                        <label class="col-md-2 form-control-label">Dosen</label>
                        <div class="col-md-10">
                            <select name="dosen_id" class="form-control readonly" readonly>
                                <option value="{{ $dosen->id }}"> {{ $dosen->namaGelar() }} </option>                                
                            </select>
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">       
                        <label class="col-sm-2 form-control-label">Nama Diklat</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nama_diklat" value="{{ old('nama_diklat') }}" placeholder="Masukan nama diklat">
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">       
                        <label class="col-sm-2 form-control-label">Penyelenggara</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="penyelenggara" value="{{ old('penyelenggara') }}" placeholder="Masukan penyelenggara">
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">       
                        <label class="col-sm-2 form-control-label">Tempat</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="tempat" value="{{ old('tempat') }}" placeholder="Masukan tempat">
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">       
                        <label class="col-sm-2 form-control-label">Tingkat</label>
                        <div class="col-sm-10">
                            <select name="tingkat" class="form-control mySelect">
                                <option value=""> - Pilih Tingkat - </option>
                                @foreach($tingkat as $key => $data)
                                    <option value="{{ $key }}" {{ old('tingkat') == $key ? 'selected' : '' }}>{{ ucwords($data) }}</option>                         
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Nomor Sertifikat</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nomor_sertifikat" value="{{ old('nomor_sertifikat') }}" placeholder="Masukan nomor sertifikat">
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Tahun Penyelenggaraan</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="tahun_penyelenggaraan" value="{{ old('tahun_penyelenggaraan') }}" placeholder="contoh: 2010, 2011, 2012 dll">
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control datepicker2" name="tanggal_mulai" value="{{ old('tanggal_mulai') }}" placeholder="Masukan tanggal mulai">
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Tanggal selesai</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control datepicker2" name="tanggal_selesai" value="{{ old('tanggal_selesai') }}" placeholder="Masukan tanggal selesai">
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">File Sertifikat</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control" name="file_sertifikat" placeholder="File Sertifikat">
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>                  
                </form>
            </div>
        </div>
    </div>
</div>

@endsection