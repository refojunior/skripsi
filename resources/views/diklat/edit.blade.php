@extends('layouts.app')

@section('title')
Edit Diklat  
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Edit Diklat</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('diklat.update', $diklat->id) }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                    @csrf
                    @method('put')
                    <div class="form-group row">       
                        <label class="col-md-2 form-control-label">Dosen</label>
                        <div class="col-md-10">
                            <select name="dosen_id" class="form-control readonly" readonly>
                                <option value="{{ $diklat->dosen->id }}"> {{ $diklat->dosen->nama }} </option>                                
                            </select>
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">       
                        <label class="col-sm-2 form-control-label">Nama Diklat</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nama_diklat" value="{{ $diklat->nama_diklat }}" placeholder="Masukan nama diklat">
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">       
                        <label class="col-sm-2 form-control-label">Penyelenggara</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="penyelenggara" value="{{ $diklat->penyelenggara }}" placeholder="Masukan penyelenggara">
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">       
                        <label class="col-sm-2 form-control-label">Tempat</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="tempat" value="{{ $diklat->tempat }}" placeholder="Masukan tempat">
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">       
                        <label class="col-sm-2 form-control-label">Tingkat</label>
                        <div class="col-sm-10">
                            <select name="tingkat" class="form-control mySelect">
                                <option value=""> - Pilih Tingkat - </option>
                                @foreach($tingkat as $key => $data)
                                    <option value="{{ $key }}" {{ $diklat->tingkat == $key ? 'selected' : '' }}>{{ $data }}</option>                         
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Nomor Sertifikat</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nomor_sertifikat" value="{{ $diklat->nomor_sertifikat }}" placeholder="Masukan nomor sertifikat">
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Tahun Penyelenggaraan</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="tahun_penyelenggaraan" value="{{ $diklat->tahun_penyelenggaraan }}" placeholder="contoh: 2010, 2011, 2012 dll">
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control datepicker2" name="tanggal_mulai" value="{{ $diklat->tanggal_mulai }}" placeholder="Masukan tanggal mulai">
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Tanggal selesai</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control datepicker2" name="tanggal_selesai" value="{{ $diklat->tanggal_selesai }}" placeholder="Masukan tanggal selesai">
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">File Sertifikat</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control" name="file_sertifikat" placeholder="File Sertifikat">
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>                  
                </form>
            </div>
        </div>
    </div>
</div>

@endsection