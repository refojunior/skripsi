@extends('layouts.app')

@section('title')
Diklat
@endsection

@section('content')


<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Diklat </h4>
            </div>
            <div class="card-body">
                <a class="btn btn-primary" data-toggle="collapse" href="#collapseForm" role="button" aria-expanded="false" aria-controls="collapseForm">
                    Advance Search
                </a>    
                <div class="line"></div>
                <div class="collapse {{ isset($_GET['search']) ? 'show' : '' }}" id="collapseForm">
                    <form action="{{ route('diklat.index') }}" method="get" class="mb-5">
                        <div class="row">                                        
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nama Dosen</label>
                                    <input type="text" class="form-control" name="name" placeholder="Nama dosen.." value="{{ (isset($_GET['search']) ? $_GET['name'] : '' ) }}">
                                </div>
                                <div class="form-group">
                                    <label>Nama Diklat</label>
                                    <input type="text" class="form-control" name="nama_diklat" placeholder="Nama diklat.." value="{{ (isset($_GET['search']) ? $_GET['nama_diklat'] : '' ) }}">
                                </div>
                                <div class="form-group">
                                    <label>Penyelenggara</label>
                                    <input type="text" class="form-control" name="penyelenggara" placeholder="Penyelenggara.." value="{{ (isset($_GET['search']) ? $_GET['penyelenggara'] : '' ) }}">
                                </div>
                                
                                <div class="form-group">       
                                    <label>Tingkat</label>
                                    <select name="tingkat" class="form-control mySelect">
                                        <option value=""> - Pilih Tingkat - </option>
                                        @foreach(getTingkat() as $key => $tingkat)
                                            <option value="{{ $tingkat }}" 
                                            @if(isset($_GET['search']) ? $_GET['name'] : '' )
                                            
                                            {{ $_GET['tingkat'] == $tingkat ? 'selected' : '' }}
                                            
                                            @endif> {{ $tingkat }}</option>                         
                                        @endforeach
                                    </select>
                                </div>
                                                        
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Tempat</label>
                                    <input type="text" class="form-control" name="tempat" placeholder="Tempat..">
                                </div>
                                <div class="form-group">
                                    <label>Tahun</label>
                                    <input type="text" class="form-control" name="tahun" placeholder="Tahun..">
                                </div>
                                <div class="form-group">
                                    <label>No Sertif</label>
                                    <input type="text" class="form-control" name="nomor_sertifikat" placeholder="No. Sertifikat..">
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="Cari" name="search" class="btn btn-primary float-right mt-3">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>


                <table class="table table-striped table-sm">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Diklat</th>
                            <th>Dosen</th>
                            <th>Penyelenggara</th>
                            <th>Tingkat</th>
                            <th>Tempat</th>
                            <th>No. Sertf</th>
                            <th>Tahun</th>
                            <th>File</th>
                            <th>Aksi</th>
                        </tr>       
                    </thead>
                    <tbody>
                        @forelse($diklat as $row)
                            <tr>
                                @if(isset($_GET['search']))
                                    <td>{{ $loop->iteration }}</td>
                                @else
                                    <td>{{ getLoopNumber($diklat, $loop->index + 1) }}</td>
                                @endif
                                <td>{{ $row->nama_diklat }}</td>
                                @if(isset($_GET['search']))
                                    <td>{{ getDosenName($row->dosen_id) }}</td>
                                @else
                                    <td>{{ $row->dosen->nama }}</td>
                                @endif
                                <td>{{ $row->penyelenggara }}</td>
                                @if(isset($_GET['search']))
                                    <td>{{ getTingkat()[$row->tingkat]  }}</td>
                                @else
                                    <td>{{ $row->getTingkat() }}</td>
                                @endif
                                <td>{{ $row->tempat }}</td> 
                                <td>{{ $row->nomor_sertifikat }}</td>
                                <td>{{ $row->tahun_penyelenggaraan }}</td>
                                <td>
                                    @if($row->file_sertifikat != null || $row->file_sertifikat != '')
                                    <a href="{{ asset('storage/diklat/'. $row->file_sertifikat) }}">Download</a>
                                    @else 
                                    null
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('diklat.indexDosen', $row->dosen_id) }}" class="text-info" title="Lihat Info Dosen"><i class="fa fa-address-card"></i></a>
                                </td>
                            </tr>
                        @empty 
                    </tbody>
                    
                    <tr>
                        <td colspan="9">Data diklat tidak ada.</td>
                    </tr>
                    @endforelse
                    
                    
                </table>
                @if(!isset($_GET['search']))
                    {{ $diklat->links() }}
                @endif
            </div>
        </div>
    </div>
</div>



@endsection