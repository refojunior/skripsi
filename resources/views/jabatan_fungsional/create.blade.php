@extends('layouts.app')

@section('title')
Tambah Jabatan Fungsional
@endsection

@section('content')

<div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Tambah Jabatan Fungsional</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('jabatan-fungsional.store') }}" method="post">
                    @csrf                    
                    <div class="form-group">       
                        <label>Dosen</label>
                        <select name="dosen_id" class="form-control readonly" readonly>
                            <option value="{{ $dosen->id }}"> {{ $dosen->namaGelar() }} </option>                                
                        </select>
                        
                    </div>

                    <div class="form-group">       
                        <label>Jabatan Fungsional</label>
                        <select name="jabatan_fungsional" class="form-control mySelect">
                            <option value=""> - Pilih Jabatan Fungsional - </option>
                            @foreach($jabatan_fungsional as $key => $jabatan)
                                <option value="{{ $jabatan }}" {{ old('jabatan_fungsional') == $jabatan ? 'selected' : '' }}>{{ $jabatan }}</option>                         
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Nomor SK</label>
                        <input type="text" name="nomor_sk" class="form-control" value="{{ old('nomor_sk') }}" placeholder="Masukan Nomor SK">
                    </div>

                    <div class="form-group">
                        <label>Terhitung Mulai Dari</label>
                        <input type="text" name="tanggal_mulai" class="form-control datepicker2" value="{{ old('tanggal_mulai') }}" placeholder="Masukan tanggal">
                    </div>

                    
                    
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>                  
                </form>
            </div>
        </div>
    </div>
</div>

@endsection