@extends('layouts.app')

@section('title')
Jabatan Fungsional {{ $dosen->namaGelar() }}
@endsection

@section('content')


<div class="row">
    <div class="col-md-12">
        @include('dosen.tabs')
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h4>Jabatan Fungsional {{ $dosen->namaGelar() }}</h4>
                </div>
                <div class="card-body">
                <a href="{{ route('jabatan-fungsional.create', $dosen->id) }}" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Tambah Jabatan Fungsional</a>
                    <table class="table">
                        <tr>
                            <th>No</th>
                            <th>Jabatan Fungsional</th>
                            <th>Nomor SK</th>
                            <th class="text-center">Terhitung Mulai Tanggal</th>                                                       
                            <th>Edit</th>
                        </tr>       
                       
                        @forelse($jabatan_fungsional as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->jabatan_fungsional }}</td>
                            <td>{{ $row->nomor_sk }}</td>
                            <td class="text-center">{{ $row->tanggal_mulai }}</td>                            
                            <td><a href="{{ route('jabatan-fungsional.edit', $row->id) }}" class="text-secondary"><i class="fa fa-edit"></i></a></td>
                            
                        </tr>
                        @empty 
                        <tr>
                            <td colspan="7">Dosen ini belum memiliki jabatan fungsional.</td>
                        </tr>
                        @endforelse
                        
                        
                    </table>
                </div>
        </div>
    </div>
</div>



@endsection