@extends('layouts.app')

@section('title')
Edit Jabatan Fungsional Dosen  
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Edit Jabatan Fungsional Dosen</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('jabatan-fungsional.update', $jabatan_fungsional->id) }}" method="post">
                    @csrf 
                    @method('put')
                    <div class="form-group">       
                        <label>Dosen</label>
                        <select name="dosen_id" class="form-control readonly" readonly>
                            <option value="{{ $jabatan_fungsional->dosen->id }}"> {{ $jabatan_fungsional->dosen->nama }} </option>                                
                        </select>
                        
                    </div>

                    <div class="form-group">       
                        <label>Jabatan Fungsional</label>
                        <select name="jabatan_fungsional" class="form-control mySelect">
                            <option value=""> - Pilih Jabatan Fungsional - </option>
                            @foreach(getJabatanFungsional() as $key => $jabatan)
                                <option value="{{ $jabatan }}" {{ $jabatan_fungsional->jabatan_fungsional == $jabatan ? 'selected' : '' }}>{{ $jabatan }}</option>                         
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Nomor SK</label>
                        <input type="text" name="nomor_sk" class="form-control" value="{{ $jabatan_fungsional->nomor_sk }}" placeholder="Masukan Nomor SK">
                    </div>

                    <div class="form-group">
                        <label>Terhitung Mulai Dari</label>
                        <input type="text" name="tanggal_mulai" class="form-control datepicker2" value="{{ $jabatan_fungsional->tanggal_mulai }}" placeholder="Masukan tanggal">
                    </div>

                   
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>                  
                </form>
            </div>
        </div>
    </div>
</div>

@endsection