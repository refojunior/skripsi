@extends('layouts.app')

@section('title')
Jabatan Fungsional
@endsection

@section('content')


<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Jabatan Fungsional </h4>
            </div>
            <div class="card-body">
                <a class="btn btn-primary" data-toggle="collapse" href="#collapseForm" role="button" aria-expanded="false" aria-controls="collapseForm">
                    Advance Search
                </a>    
                <div class="line"></div>
                <div class="collapse {{ isset($_GET['search']) ? 'show' : '' }}" id="collapseForm">
                    <form action="{{ route('jabatan-fungsional.index') }}" method="get" class="mb-5">
                        <div class="row">                                        
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nama Dosen</label>
                                    <input type="text" class="form-control" name="name" placeholder="Nama dosen.." value="{{ (isset($_GET['search']) ? $_GET['name'] : '' ) }}">
                                </div>
                                <div class="form-group">       
                                    <label>Jabatan Fungsional</label>
                                    <select name="jabatan_fungsional" class="form-control mySelect">
                                        <option value=""> - Pilih Jabatan Fungsional - </option>
                                        @foreach(getJabatanFungsional() as $key => $jabatan)
                                            <option value="{{ $jabatan }}" 
                                            @if(isset($_GET['search']) ? $_GET['name'] : '' )
                                            
                                            {{ $_GET['jabatan_fungsional'] == $jabatan ? 'selected' : '' }}
                                            
                                            @endif> {{ $jabatan }}</option>                         
                                        @endforeach
                                    </select>
                                </div>
                                                         
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nomor SK</label>
                                    <input type="text" class="form-control" name="nomor_sk" value="{{ (isset($_GET['search']) ? $_GET['nomor_sk'] : '' ) }}" placeholder="Nomor SK..">
                                </div>   
                                
                                <div class="form-group">
                                    <label>Tanggal Mulai</label>
                                    <input type="text" class="form-control datepicker2" name="tanggal_mulai" value="{{ (isset($_GET['search']) ? $_GET['tanggal_mulai'] : '' ) }}" placeholder="Tanggal mulai..">
                                </div>                               
                                <div class="form-group">
                                    <input type="submit" value="Cari" name="search" class="btn btn-primary float-right mt-3">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <table class="table table-sm table-striped">
                    <tr>
                        <th>No</th>
                        <th>Jabatan Fungsional</th>
                        <th>Dosen</th>
                        <th>Nomor SK</th>
                        <th class="text-center">Terhitung Mulai Tanggal</th>                                             
                        <th>Aksi</th>
                    </tr>       
                    
                    @forelse($jabatan_fungsional as $row)
                    <tr>
                        @if(isset($_GET['search']))
                            <td>{{ $loop->iteration }}</td>
                        @else
                            <td>{{ getLoopNumber($jabatan_fungsional, $loop->index + 1) }}</td>
                        @endif
                            <td>{{ $row->jabatan_fungsional }}</td>
                        @if(isset($_GET['search']))
                            <td>{{ getDosenName($row->dosen_id) }}</td>
                        @else
                            <td>{{ $row->dosen->nama }}</td>
                        @endif
                        
                        <td>{{ $row->nomor_sk }}</td>
                        <td class="text-center">{{ $row->tanggal_mulai }}</td>                       
                        <td>
                            <ul class="d-flex action-button">                                            
                                <li><a href="{{ route('jabatan-fungsional.indexDosen', $row->dosen_id) }}" class="text-primary" title="Lihat Detail Dosen"><i class="fa fa-user-tie"></i></a></li>                                            
                            </ul>
                        </td>
                        
                        
                    </tr>
                    @empty 
                    <tr>
                        <td colspan="7">Data jabatan fungsional dosen tidak ada.</td>
                    </tr>
                    @endforelse
                    
                    
                </table>
                @if(!isset($_GET['search']))
                {{ $jabatan_fungsional->links() }}
                @endif
            </div>
        </div>
    </div>
</div>



@endsection