@extends('layouts.app')

@section('title', 'Tambah Kompetensi')


@section('content')

<div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Tambah Kompetensi</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('kompetensi.store') }}" method="post">
                    @csrf
                    <div class="form-group">       
                        <label>Nama Kompetensi</label>
                        <input type="text" placeholder="contoh: Rekayasa perangkat lunak, basis data dll" class="form-control" name="nama_kompetensi" value="{{ old('nama_kompetensi') }}">
                    </div> 
                    <div class="form-group">
                        <label>Deskripsi</label>
                        <textarea name="deskripsi" class="form-control">{{ old('deskripsi') }}</textarea>
                    </div> 
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>                  
                </form>
            </div>
        </div>
    </div>
</div>

@endsection