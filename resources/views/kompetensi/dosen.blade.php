@extends('layouts.app')

@section('title')
Dosen Kompetensi {{ $kompetensi->nama_kompetensi }}
@endsection


@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Data Dosen Pada Kompetensi {{ $kompetensi->nama_kompetensi }}</h4>
            </div>
            <div class="card-body mt-3">                
                <table class="table table-sm table-striped datatable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>NIDN</th>
                            <th>Nama</th>
                            <th>Kompetensi</th>
                            <th>Prodi</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($dosen as $row)                        
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->nidn }}</td>
                            <td>{{ $row->nama }}</td>
                            <td>{{ $kompetensi->nama_kompetensi }}</td>
                            <td>{{ $row->prodi->nama_prodi }}</td>
                            <td>
                                <a href="{{ route('kompetensi-dosen.index', $row->id) }}" class="text-info" title="Lihat Info Dosen"><i class="fa fa-address-card"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection