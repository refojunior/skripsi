@extends('layouts.app')

@section('title', 'Edit Kompetensi')


@section('content')

<div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Edit Kompetensi</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('kompetensi.update', $kompetensi->id) }}" method="post">
                    @csrf
                    @method('put')
                    <div class="form-group">       
                        <label>Nama Kompetensi</label>
                        <input type="text" placeholder="contoh: Rekayasa perangkat lunak, basis data dll" class="form-control" name="nama_kompetensi" value="{{ $kompetensi->nama_kompetensi }}">
                    </div> 
                    <div class="form-group">
                        <label>Deskripsi</label>
                        <textarea name="deskripsi" class="form-control">{{ $kompetensi->deskripsi }}</textarea>
                    </div> 
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>                  
                </form>
            </div>
        </div>
    </div>
</div>

@endsection