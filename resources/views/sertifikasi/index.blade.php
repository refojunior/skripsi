@extends('layouts.app')

@section('title')
Sertifikasi
@endsection

@section('content')


<div class="row">
    <div class="col-md-12">
        <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h4>Sertifikasi </h4>
                </div>
                <div class="card-body">
                    <a class="btn btn-primary" data-toggle="collapse" href="#collapseForm" role="button" aria-expanded="false" aria-controls="collapseForm">
                        Advance Search
                    </a>    
                    <div class="line"></div>
                    <div class="collapse {{ isset($_GET['search']) ? 'show' : '' }}" id="collapseForm">
                        <form action="{{ route('sertifikasi') }}" method="get" class="mb-5">
                            <div class="row">                                        
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nama Dosen</label>
                                        <input type="text" class="form-control" name="name" placeholder="Nama dosen.." value="{{ (isset($_GET['search']) ? $_GET['name'] : '' ) }}">
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Nama Sertifikasi</label>
                                        <input type="text" class="form-control" name="nama_sertifikasi" value="{{ (isset($_GET['search']) ? $_GET['nama_sertifikasi'] : '' ) }}" placeholder="Nama sertifikasi..">
                                    </div>
                                    <div class="form-group">
                                        <label>Nomor Sertifikasi</label>
                                        <input type="text" class="form-control" name="nomor_sertifikasi" value="{{ (isset($_GET['search']) ? $_GET['nomor_sertifikasi'] : '' ) }}" placeholder="Nomor sertifikasi..">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tahun Sertifikasi</label>
                                        <input type="number" class="form-control" name="tahun_sertifikasi" value="{{ (isset($_GET['search']) ? $_GET['tahun_sertifikasi'] : '' ) }}" placeholder="Tahun sertifikasi..">
                                    </div>

                                    <div class="form-group">
                                        <label>Nomor Peserta</label>
                                        <input type="text" class="form-control" name="nomor_peserta" value="{{ (isset($_GET['search']) ? $_GET['nomor_peserta'] : '' ) }}" placeholder="Nomor peserta..">
                                    </div>
                                    <div class="form-group">
                                        <label>Nomor Registrasi</label>
                                        <input type="text" class="form-control" name="nomor_registrasi" value="{{ (isset($_GET['search']) ? $_GET['nomor_registrasi'] : '' ) }}" placeholder="Nomor registrasi..">
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" value="Cari" name="search" class="btn btn-primary float-right mt-3">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <table class="table table-sm table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Dosen</th>
                                <th>Nama Sertifikasi</th>
                                <th>No. Sertifikasi</th>
                                <th>Tahun</th>
                                <th>No. Peserta </th>
                                <th>No. Registrasi</th> 
                                <th>File Sertifikasi</th>                           
                                <th>Aksi</th>
                            </tr>   
                        </thead>
                        <tbody>
                            @foreach($sertifikasi as $row)
                            <tr>
                                @if(isset($_GET['search']))
                                    <td>{{ $loop->iteration }}</td>
                                @else
                                    <td>{{ getLoopNumber($sertifikasi, $loop->index + 1) }}</td>
                                @endif
                                @if(isset($_GET['search']))
                                    <td>{{ getDosenName($row->dosen_id) }}</td>
                                @else
                                    <td>{{ $row->dosen->nama }}</td>
                                @endif
                                <td>{{ $row->nama_sertifikasi }}</td>
                                <td>{{ $row->nomor_sertifikasi }}</td>
                                <td>{{ $row->tahun_sertifikasi }}</td>
                                <td>{{ $row->nomor_peserta }}</td>
                                <td>{{ $row->nomor_registrasi }}</td>
                                <td><a href="{{ asset('storage/sertifikasi/'.$row->file_sertifikasi) }}">Download</td>
                                <td>                                    
                                    <ul class="d-flex action-button">                                            
                                        <li><a href="{{ route('sertifikasi.index', [$row->dosen_id]) }}" class="text-primary" title="Lihat Detail Dosen"><i class="fa fa-user-tie"></i></a></li>                                            
                                    </ul>

                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    @if(!isset($_GET['search']))
                    {{ $sertifikasi->links() }}
                    @endif
                </div>
        </div>
    </div>
</div>

@endsection