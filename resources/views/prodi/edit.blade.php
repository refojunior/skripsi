@extends('layouts.app')

@section('title', 'Tambah Program Studi')


@section('content')

<div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Edit Program Studi</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('prodi.update', $prodi->id) }}" method="post">
                    @csrf
                    @method('put')
                    <div class="form-group">       
                        <label>Nama Prodi</label>
                        <input type="text" placeholder="Nama Prodi" class="form-control" name="nama_prodi" value="{{ $prodi->nama_prodi }}">
                    </div>  
                    <div class="form-group">       
                        <label>Jenjang Pendidikan</label>
                        <input type="text" placeholder="Jenjang Pendidikan" class="form-control" name="jenjang_pendidikan" value="{{ $prodi->jenjang_pendidikan }}">
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>                  
                </form>
            </div>
        </div>
    </div>
</div>

@endsection