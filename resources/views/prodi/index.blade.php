@extends('layouts.app')

@section('title', 'Program Studi')


@section('content')

<div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Data Program Studi</h4>
            </div>
            <div class="card-body">
                <a href="{{ route('prodi.create') }}" class="btn btn-primary btn-sm mb-4">Tambah Prodi</a>
                <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Prodi</th>
                            <th class="text-center">Jenjang Pendidikan</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($prodi as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->nama_prodi }}</td>
                            <td class="text-center">{{ $row->jenjang_pendidikan }}</td>
                            <td>
                                <form action="{{ route('prodi.destroy', $row->id) }}" method="post">
                                    <ul class="d-flex action-button">
                                        <li><a href="{{ route('prodi.dosen', $row->id) }}" class="text-primary" title="Lihat dosen prodi ini"><i class="fa fa-user-tie"></i></a></li>
                                        <li><a href="{{ route('prodi.edit', $row->id) }}" class="text-secondary" title="Edit"><i class="fa fa-edit"></i></a></li>
                                        @csrf
                                        @method('delete')
                                        <li><button type="submit" class="text-danger btn-submit" onclick="return confirm('Yakin akan hapus data?')" ><i class="fa fa-trash"></i></button></li>
                                    </ul>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection