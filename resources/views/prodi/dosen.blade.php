@extends('layouts.app')

@section('title')
Dosen Prodi {{ $prodi->nama_prodi }}
@endsection


@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Data Dosen Pada Program Studi {{ $prodi->nama_prodi }}</h4>
            </div>
            <div class="card-body">
                <a href="{{ route('print.dosen-prodi', $prodi->id) }}" class="btn btn-outline-primary btn-sm mb-4"><i class="fa fa-print"></i> Cetak</a> &nbsp; <a href="{{ route('export.dosen-prodi', $prodi->id) }}" class="btn btn-outline-primary btn-sm mb-4"><i class="fa fa-table"></i> Export Excel</a>
                <table class="table table-striped datatable table-sm">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>NIDN</th>
                            <th>Nama</th>
                            <th>Prodi</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($dosen as $row)                        
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->nidn }}</td>
                            <td>{{ $row->nama }}</td>
                            <td>{{ $row->prodi->nama_prodi }}</td>
                            <td>
                                <a href="{{ route('dosen.show', $row->id) }}" class="text-info" title="Lihat Info Dosen"><i class="fa fa-address-card"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection