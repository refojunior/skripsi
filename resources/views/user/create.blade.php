@extends('layouts.app')

@section('title')
Tambah User
@endsection

@section('content')


<div class="row">
    <div class="col-md-7">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Tambah User</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('user.store') }}" method="post">
                    @csrf 
                    
                    <div class="form-group">       
                        <label>Username</label>
                        <input type="text" placeholder="Masukan username" class="form-control" name="username" value="{{ old('username') }}" required>
                    </div>

                    <div class="form-group">       
                        <label>Dosen <small>Kosongkan user jika bukan dosen</small></label>
                        <select name="dosen_id" class="mySelect form-control">
                            <option value=""> - Pilih Dosen - </option>
                            @foreach($dosen as $data)
                                <option value="{{ $data->id }}" {{ (old('dosen_id') == $data->id) ? 'selected' : '' }}>{{ $data->nama }}</option>                         
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">       
                        <label>Password</label>
                        <input type="password" placeholder="Masukan password" class="form-control" name="password" required>
                    </div>

                    <div class="form-group">       
                        <label>Re-type Password</label>
                        <input type="password" placeholder="Masukan password" class="form-control" name="password_confirmation" required>
                    </div>

                    <div class="form-group">       
                        <label>Level</label>
                        <select name="level" class="mySelect form-control" required>
                            <option value=""> - Pilih Level - </option>
                            <option value="1" {{ (old('level') == '1') ? 'selected' : '' }}>Admin</option>
                            <option value="0" {{ (old('level') == '0') ? 'selected' : '' }}>Kaprodi</option>
                        </select>
                    </div>

                    <div class="form-group mt-4">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div> 
                </form>
            </div>
        </div>
    </div>
</div>

@endsection