@extends('layouts.app')

@section('title')
User
@endsection

@section('content')


<div class="row">
    <div class="col-md-12">
        <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h4>User </h4>
                </div>
                <div class="card-body">
                    <a href="{{ route('user.create') }}" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Tambah User</a>
                    <table class="table datatable table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Dosen</th>
                                <th>Username</th>
                                <th>Level</th>                            
                                <th>Aksi</th>
                            </tr>   
                        </thead>
                        <tbody>
                            @foreach($users as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ ($row->dosen_id != null) ? $row->dosen->nama : '' }}</td>
                                <td>{{ $row->username }}</td>
                                <td>{{ $row->getLevel() }}</td>
                                <td>
                                    <ul class="d-flex action-button">                                            
                                        <li><a href="{{ route('user.edit', $row->id) }}" class="text-secondary" title="Edit User"><i class="fa fa-edit"></i></a></li>                                            
                                    </ul>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
        </div>
    </div>
</div>

@endsection