<ul class="nav nav-tabs">
    <li class="nav-item">
        <a class="nav-link {{ ($tab == 1) ? 'active' : '' }}" href="{{ route('dosen.show', $dosen->id) }}">Biodata</a>
    </li>            
    <li class="nav-item">
        <a class="nav-link {{ ($tab == 2) ? 'active' : '' }}" href="{{ route('pendidikan.index', $dosen->id) }}">Pendidikan</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ ($tab == 3) ? 'active' : '' }}" href="{{ route('kompetensi-dosen.index', $dosen->id) }}">Kompetensi</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ ($tab == 4) ? 'active' : '' }}" href="{{ route('jabatan-fungsional.indexDosen', $dosen->id) }}">Jabatan Fungsional</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ ($tab == 5) ? 'active' : '' }}" href="{{ route('diklat.indexDosen', $dosen->id) }}">Diklat</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ ($tab == 6) ? 'active' : '' }}" href="{{ route('penelitian-pengabdian.indexDosen', $dosen->id) }}">Penelitian & Pengabdian</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ ($tab == 7) ? 'active' : '' }}" href="{{ route('pengajaran.index', $dosen->id) }}">Pengajaran</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ ($tab == 8) ? 'active' : '' }}" href="{{ route('sertifikasi.index', $dosen->id) }}">Sertifikasi</a>
    </li>
</ul>