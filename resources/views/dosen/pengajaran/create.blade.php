@extends('layouts.app')

@section('title')
Tambah Riwayat Pengajaran {{ $dosen->namaGelar() }}
@endsection

@section('content')


<div class="row">
    <div class="col-md-12">
        @include('dosen.tabs')
            <div class="card">

                <div class="card-header d-flex align-items-center">                           
                    <h4>Tambah Riwayat Pengajaran {{ $dosen->namaGelar() }}</h4>
                </div>
                <div class="card-body col-md-9">
                    <form action="{{ route('pengajaran.store', $dosen->id) }}" method="post" class="form-horizontal" >
                        @csrf                                   
                        <div class="form-group row">
                            <label class="col-sm-2 form-control-label">Kode Kelas</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="kode_kelas" value="{{ old('kode_kelas') }}">
                            </div>
                        </div>
                        <div class="line"></div>

                        <div class="form-group row">
                            <label class="col-sm-2 form-control-label">Matakuliah</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="matakuliah" value="{{ old('matakuliah') }}">
                            </div>
                        </div>
                        <div class="line"></div>

                        <div class="form-group row">       
                            <label class="col-sm-2 form-control-label">Tahun Ajar</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="tahun_ajar" value="{{ old('tahun_ajar') }}" placeholder="contoh: 2019/2020">
                            </div>
                        </div>
                        <div class="line"></div>

                        <div class="form-group row">       
                            <label class="col-sm-2 form-control-label">Semester</label>
                            <div class="col-sm-10">
                                <select name="semester" class="form-control">
                                    <option value=""> - Pilih Semester - </option>
                                    <option value="ganjil" {{ old('semester') == 'ganjil' ? 'selected' : '' }}>Ganjil</option>
                                    <option value="genap" {{ old('semester') == 'genap' ? 'selected' : '' }}>Genap</option>
                                </select>
                            </div>
                        </div>
                        <div class="line"></div>
                    
                        <div class="form-group row">
                            <div class="col-md-12">                            
                                <input type="submit" class="btn btn-primary float-right" value="Simpan">
                            </div>
                        </div>
                    </form>
                </div>
        </div>
    </div>
</div>



@endsection