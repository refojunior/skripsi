@extends('layouts.app')

@section('title')
Riwayat Pengajaran {{ $dosen->namaGelar() }}
@endsection

@section('content')


<div class="row">
    <div class="col-md-12">
        @include('dosen.tabs')
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h4>Riwayat Pengajaran {{ $dosen->namaGelar() }}</h4>
                </div>
                <div class="card-body">
                <a href="{{ route('pengajaran.create', $dosen->id) }}" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Tambah Pengajaran</a>
                    <table class="table">
                        <tr>
                            <th>No</th>
                            <th>Kode Kelas</th>
                            <th>Mata Kuliah</th>                            
                            <th>Tahun Ajar</th>
                            <th>Semester </th>
                            <th>Aksi</th>
                        </tr>       
                       
                        @forelse($pengajaran as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->kode_kelas }}</td>
                                <td>{{ $row->matakuliah }}</td>
                                <td>{{ $row->tahun_ajar }}</td>
                                <td>{{ $row->semester }}</td>
                                <td>                                    
                                    <ul class="d-flex action-button">                                            
                                        <li><a href="{{ route('pengajaran.edit', [$dosen->id, $row->id]) }}" class="text-secondary" title="Edit"><i class="fa fa-edit"></i></a></li>                                            
                                    </ul>

                                </td>
                            </tr>
                        @empty 
                            <tr>
                                <td colspan="7">Dosen ini belum memiliki riwayat pengajaran</td>
                            </tr>
                        @endforelse
                        
                        
                    </table>
                </div>
        </div>
    </div>
</div>



@endsection