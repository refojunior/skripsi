@extends('layouts.app')

@section('title', 'Dosen')

@section('content')


<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Data Dosen</h4>
            </div>
            <div class="card-body">
                <a href="{{ route('dosen.create') }}" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Tambah Dosen</a> &nbsp; <a href="{{ route('dosen.print') }}" class="btn btn-outline-primary btn-sm mb-4"><i class="fa fa-print"></i> Cetak</a> &nbsp; <a href="{{ route('export.dosen') }}" class="btn btn-outline-primary btn-sm mb-4"><i class="fa fa-table"></i> Export Excel</a>
                <table class="table table-sm table-striped datatable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>NIDN</th>
                            <th>Nama Dosen</th>                           
                            <th>Prodi</th>
                            <th>Tipe</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($dosen as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->nidn }}</td>
                            <td>{{ $row->nama }}</td>                            
                            <td>{{ $row->prodi->nama_prodi }}</td>
                            <td>{{ strtoupper($row->tipe_dosen) }}</td>
                            <td class="text-center">{!! $row->getStatus() !!}</td>
                            <td>
                                <form action="{{ route('dosen.destroy', $row->id) }}" method="post">
                                    <ul class="d-flex action-button">
                                        <li><a href="{{ route('dosen.show', $row->id) }}" class="text-info" title="Lihat Detail"><i class="fa fa-address-card"></i></a></li>
                                        <li><a href="{{ route('dosen.edit', $row->id) }}" class="text-secondary" title="Edit"><i class="fa fa-edit"></i></a></li>
                                        @csrf
                                        @method('delete')
                                        <!-- <li><button type="submit" class="text-danger btn-submit" onclick="return confirm('Yakin akan hapus data?')" ><i class="fa fa-trash"></i></button></li> -->
                                    </ul>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection