@extends('layouts.app')

@section('title')
Kompetensi {{ $dosen->nama }}
@endsection

@section('content')


<div class="row">
    <div class="col-md-12">
        @include('dosen.tabs')
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h4>Kompetensi {{ $dosen->namaGelar() }}</h4>
                </div>
                <div class="card-body">
                <a href="#" data-toggle="modal" data-target="#addKompetensiDosenModal" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Tambah Kompetensi Dosen</a>
                    <table class="table">
                        <tr>
                            <th>No</th>
                            <th>Nama Kompetensi</th>
                            <th>Keterangan</th>
                            <th>Aksi</th>
                        </tr>       
                       
                        @forelse($kompetensi_dosen as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->nama_kompetensi }}</td>
                            <td>{{ $row->pivot->keterangan }}</td>
                            <td>
                                <form action="{{ route('kompetensi-dosen.destroy', [$dosen->id, $row->pivot->kompetensi_id]) }}" method="post">
                                    <ul class="d-flex action-button">
                                        @csrf
                                        @method('delete')
                                        <li><button type="submit" class="text-danger btn-submit" onclick="return confirm('Yakin akan hapus data?')" ><i class="fa fa-trash"></i></button></li>
                                    </ul>
                                </form>
                            </td>
                        </tr>
                        @empty 
                        <tr>
                            <td colspan="3">Dosen ini belum memiliki kompetensi</td>
                        </tr>
                        @endforelse
                        
                        
                    </table>
                </div>
        </div>
    </div>
</div>


<div class="modal fade" id="addKompetensiDosenModal" role="dialog" aria-labelledby="addKompetensiDosenModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addKompetensiDosenModalLabel">Tambah Kompetensi Dosen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('kompetensi-dosen.store', $dosen->id) }}" class="form-horizontal" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group row form-select">
                        <label class="col-sm-3">Nama Kompetensi</label>
                        <div class="col-sm-9">
                            <select name="kompetensi_id" class="form-control mySelect">
                                <option value=""> - Pilih Kompetensi - </option>
                                @foreach($data_kompetensi as $key => $data)
                                    <option value="{{ $data->id }}" {{ old('kompetensi_id') == $data->id ? 'selected' : '' }}>{{ $data->nama_kompetensi }}</option>                         
                                @endforeach
                            </select>
                            
                        </div>
                        
                    </div>
                    <div class="form-group row form-select">
                        <label class="col-sm-3">Keterangan</label>
                        <div class="col-sm-9">
                            <input type="text" name="keterangan" class="form-control" value="{{ old('keterangan') }}">
                        </div>                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection