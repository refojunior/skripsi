@extends('layouts.app')

@section('title')
Riwayat Sertifikasi {{ $dosen->namaGelar() }}
@endsection

@section('content')


<div class="row">
    <div class="col-md-12">
        @include('dosen.tabs')
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h4>Riwayat Sertifikasi {{ $dosen->namaGelar() }}</h4>
                </div>
                <div class="card-body">
                <a href="{{ route('sertifikasi.create', $dosen->id) }}" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Tambah Sertifikasi</a>
                    <table class="table">
                        <tr>
                            <th>No</th>
                            <TH>Nama sertifikasi</TH>
                            <th>Nomor Sertifikasi</th>
                            <th>Tahun</th>
                            <th>No. Peserta </th>
                            <th>No. Registrasi</th> 
                            <th>File Sertifikasi</th>                           
                            <th>Aksi</th>
                        </tr>       
                       
                        @forelse($sertifikasi as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->nama_sertifikasi }}</td>
                                <td>{{ $row->nomor_sertifikasi }}</td>
                                <td>{{ $row->tahun_sertifikasi }}</td>
                                <td>{{ $row->nomor_peserta }}</td>
                                <td>{{ $row->nomor_registrasi }}</td>
                                <td><a href="{{ asset('storage/sertifikasi/'.$row->file_sertifikasi) }}">Download</td>
                                <td>                                    
                                    <ul class="d-flex action-button">                                            
                                        <li><a href="{{ route('sertifikasi.edit', [$dosen->id, $row->id]) }}" class="text-secondary" title="Edit"><i class="fa fa-edit"></i></a></li>                                            
                                    </ul>

                                </td>
                            </tr>
                        @empty 
                            <tr>
                                <td colspan="7">Dosen ini belum memiliki riwayat sertifikasi</td>
                            </tr>
                        @endforelse
                        
                        
                    </table>
                </div>
        </div>
    </div>
</div>



@endsection