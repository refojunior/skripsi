@extends('layouts.app')

@section('title')
Edit Riwayat Sertifikasi {{ $dosen->namaGelar() }}
@endsection

@section('content')


<div class="row">
    <div class="col-md-12">
        @include('dosen.tabs')
            <div class="card">

                <div class="card-header d-flex align-items-center">                           
                    <h4>Edit Riwayat Sertifikasi {{ $dosen->namaGelar() }}</h4>
                </div>
                <div class="card-body col-md-7">
                    <form action="{{ route('sertifikasi.update', [$dosen->id, $sertifikasi->id ]) }}" method="post" class="form-horizontal" enctype="multipart/form-data">
                        @csrf 
                        @method('put')

                        <div class="form-group row">
                            <label class="col-sm-2 form-control-label">Nama Sertifikasi</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="nama_sertifikasi" value="{{ $sertifikasi->nama_sertifikasi }}">
                            </div>
                        </div>
                        <div class="line"></div>

                        <div class="form-group row">
                            <label class="col-sm-2 form-control-label">Nomor Sertifikasi</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="nomor_sertifikasi" value="{{ $sertifikasi->nomor_sertifikasi }}">
                            </div>
                        </div>
                        <div class="line"></div>

                        <div class="form-group row">       
                            <label class="col-sm-2 form-control-label">Tahun Sertifikasi</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" name="tahun_sertifikasi" value="{{ $sertifikasi->tahun_sertifikasi }}" placeholder="contoh: 2019, 2020">
                            </div>
                        </div>
                        <div class="line"></div>

                        <div class="form-group row">
                            <label class="col-sm-2 form-control-label">Nomor Peserta</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="nomor_peserta" value="{{ $sertifikasi->nomor_peserta }}">
                            </div>
                        </div>
                        <div class="line"></div>

                        <div class="form-group row">
                            <label class="col-sm-2 form-control-label">Nomor Registrasi</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="nomor_registrasi" value="{{ $sertifikasi->nomor_registrasi }}">
                            </div>
                        </div>
                        <div class="line"></div>

                        
                        <div class="form-group row">       
                            <label class="col-sm-2 form-control-label">File Sertifikasi</label>
                            <div class="col-sm-10">
                                <input type="file" class="form-control" name="file_sertifikasi" value="{{ $sertifikasi->file_sertifikasi }}" >
                            </div>
                        </div>
                        <div class="line"></div>

                        <div class="form-group row">
                            <div class="col-md-12">                            
                                <input type="submit" class="btn btn-primary float-right" value="Simpan">
                            </div>
                        </div>
                    </form>
                </div>
        </div>
    </div>
</div>



@endsection