@extends('layouts.app')

@section('title')
Tambah Pendidikan {{ $dosen->nama }}
@endsection

@section('content')


<div class="row">
    <div class="col-md-12">
        @include('dosen.tabs')
            <div class="card">

                <div class="card-header d-flex align-items-center">                           
                    <h4>Tambah Pendidikan {{ $dosen->namaGelar() }}</h4>
                </div>
                <div class="card-body">
                    <form action="{{ route('pendidikan.store', $dosen->id) }}" method="post" class="form-horizontal" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="dosen_id" value="{{ $dosen->id }}">
                        <div class="form-group row">       
                            <label class="col-sm-2 form-control-label">Almamater</label>
                            <div class="col-sm-10">
                                <select name="almamater_id" class="form-control mySelect">
                                    <option value=""> - Pilih Almamater - </option>
                                    @foreach($almamater as $key => $data)
                                        <option value="{{ $data->id }}" {{ old('almamater_id') == $data->id ? 'selected' : '' }}>{{ $data->nama_almamater }}</option>                         
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="line"></div>

                        <div class="form-group row">
                            <label class="col-sm-2 form-control-label">Jenjang Pendidikan</label>
                            <div class="col-sm-10">
                                <select name="jenjang_pendidikan" class="form-control mySelect">
                                    <option value=""> - Pilih Jenjang Pendidikan - </option>
                                    @foreach(getJenjangPendidikan() as $pendidikan)
                                        <option value="{{ $pendidikan }}" {{ old('jenjang_pendidikan') == $pendidikan ? 'selected' : '' }}>{{ $pendidikan }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="line"></div>


                        <div class="form-group row">
                            <label class="col-sm-2 form-control-label">Bidang Studi</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="bidang_studi" value="{{ old('bidang_studi') }}">
                            </div>
                        </div>
                        <div class="line"></div>

                        <div class="form-group row">       
                            <label class="col-sm-2 form-control-label">Gelar</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="gelar" value="{{ old('gelar') }}" placeholder="contoh: S.Kom, M.Kom, SE dll">
                            </div>
                        </div>
                        <div class="line"></div>

                        <div class="form-group row">       
                            <label class="col-sm-2 form-control-label">Tahun Lulus</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" name="tahun_lulus" value="{{ old('tahun_lulus') }}" placeholder="contoh: 2006, 2007, 2008 dll">
                            </div>
                        </div>
                        <div class="line"></div>

                        <div class="form-group row">       
                            <label class="col-sm-2 form-control-label">Ijazah</label>
                            <div class="col-sm-10">
                                <input type="file" class="form-control" name="ijazah" value="{{ old('ijazah') }}" >
                            </div>
                        </div>
                        <div class="line"></div>

                        <div class="form-group row">       
                            <label class="col-sm-2 form-control-label">Transkrip</label>
                            <div class="col-sm-10">
                                <input type="file" class="form-control" name="transkrip" value="{{ old('transkrip') }}" >
                            </div>
                        </div>
                        <div class="line"></div>

                        <div class="form-group row">
                            <div class="col-md-12">                            
                                <input type="submit" class="btn btn-primary float-right" value="Simpan">
                            </div>
                        </div>
                    </form>
                </div>
        </div>
    </div>
</div>



@endsection