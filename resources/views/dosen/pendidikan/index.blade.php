@extends('layouts.app')

@section('title')
Pendidikan {{ $dosen->nama }}
@endsection

@section('content')


<div class="row">
    <div class="col-md-12">
        @include('dosen.tabs')
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h4>Pendidikan {{ $dosen->namaGelar() }}</h4>
                </div>
                <div class="card-body">
                <a href="{{ route('pendidikan.create', $dosen->id) }}" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Tambah Pendidikan Dosen</a>
                    <table class="table">
                        <tr>
                            <th>No</th>
                            <th>Nama Almamater</th>
                            <th>Jenjang Pendidikan</th>
                            <th>Bidang Studi</th>
                            <th>Gelar</th>
                            <th>Lulus</th>
                            <th>Ijazah</th>
                            <th>Transkrip</th>
                            <th>Hapus</th>
                        </tr>       
                       
                        @forelse($pendidikan as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->almamater->nama_almamater }}</td>
                            <td>{{ $row->jenjang_pendidikan }}</td>
                            <td>{{ $row->bidang_studi }}</td>
                            <td>{{ $row->gelar }}</td>
                            <td>{{ $row->tahun_lulus }}</td>
                            <td><a href="{{ url('storage/ijazah/'.$row->ijazah) }}" target="_blank">Download</a></td>
                            <td><a href="{{ url('storage/transkrip/'.$row->transkrip) }}" target="_blank">Download</a></td>
                            <td>
                                <form action="{{ route('pendidikan.destroy', $row->id) }}" method="post">
                                    <ul class="d-flex action-button">
                                        @csrf
                                        @method('delete')
                                        <li><a href="{{ route('pendidikan.edit', $row->id) }}" class="text-secondary" title="Edit"><i class="fa fa-edit"></i></a></li>
                                        <li><button type="submit" class="text-danger btn-submit" onclick="return confirm('Yakin akan hapus data?')" ><i class="fa fa-trash"></i></button></li>
                                    </ul>
                                </form>
                            </td>
                        </tr>
                        @empty 
                        <tr>
                            <td colspan="7">Dosen ini belum memiliki pendidikan</td>
                        </tr>
                        @endforelse
                        
                        
                    </table>
                </div>
        </div>
    </div>
</div>



@endsection