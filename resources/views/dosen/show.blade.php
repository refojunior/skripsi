@extends('layouts.app')

@section('title')
{{ $dosen->namaGelar() }}
@endsection

@section('content')


<div class="row">
    <div class="col-md-12">
        @include('dosen.tabs')
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h4>Biodata {{ $dosen->namaGelar() }}</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-8">
                            <table class="table">
                                <tr>
                                    <th style="border-top:0">NIDN</th>
                                    <td style="border-top:0">:</td>
                                    <td style="border-top:0">{{ $dosen->nidn }}</td>
                                </tr>
                                <tr>
                                    <th>Nama</th>
                                    <td>:</td>
                                    <td>{{ $dosen->nama }}</td>
                                </tr>
                                <tr>
                                    <th>Gelar Depan</th>
                                    <td>:</td>
                                    <td>{{ $dosen->gelar_depan }}</td>
                                </tr>
                                <tr>
                                    <th>Gelar Belakang</th>
                                    <td>:</td>
                                    <td>{{ $dosen->gelar_belakang }}</td>
                                </tr>
                                <tr>
                                    <th>Nama dengan gelar</th>
                                    <td>:</td>
                                    <td>{{ $dosen->namaGelar() }}</td>
                                </tr>
                                <tr>
                                    <th>NIK</th>
                                    <td>:</td>
                                    <td>{{ $dosen->nik }}</td>
                                </tr>
                                <tr>
                                    <th>Kewarganegaraan</th>
                                    <td>:</td>
                                    <td>{{ $dosen->kewarganegaraan }}</td>
                                </tr>
                                <tr>
                                    <th>Tempat & Tanggal Lahir</th>
                                    <td>:</td>
                                    <td>{{ $dosen->tempat_lahir }}, {{ $dosen->tanggal_lahir }}</td>
                                </tr>
                                <tr>
                                    <th>Jenis Kelamin</th>
                                    <td>:</td>
                                    <td>{{ $dosen->getKelamin() }}</td>
                                </tr>
                                <tr>
                                    <th>Agama</th>
                                    <td>:</td>
                                    <td>{{ $dosen->agama }}</td>
                                </tr>
                                <tr>
                                    <th>Prodi</th>
                                    <td>:</td>
                                    <td><a href="{{ route('prodi.dosen', $dosen->prodi_id) }}"><span class="badge badge-info">{{ $dosen->prodi->nama_prodi }}</span></a></td>
                                </tr>
                                <tr>
                                    <th>Bidang Ilmu</th>
                                    <td>:</td>
                                    <td><a href="{{ route('bidang-ilmu.dosen', $dosen->bidang_ilmu_id) }}"><span class="badge badge-primary">{{ $dosen->bidangIlmu->nama_bidang_ilmu }}</span></a></td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>:</td>
                                    <td>{{ $dosen->email }}</td>
                                </tr>
                                <tr>
                                    <th>Alamat</th>
                                    <td>:</td>
                                    <td>{{ $dosen->alamat }}</td>
                                </tr>
                                <tr>
                                    <th>Golongan</th>
                                    <td>:</td>
                                    <td>{{ $dosen->golongan }}</td>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <td>:</td>
                                    <td>{!! $dosen->getStatus() !!}</td>
                                </tr>
                                <tr>
                                    <th>Keterangan</th>
                                    <td>:</td>
                                    <td>{{ $dosen->keterangan }}</td>
                                </tr>
        
                                
                            </table>

                            <a href="{{ route('dosen.edit', $dosen->id) }}" class="btn btn-secondary"><i class="fa fa-cog"></i> Edit</a>
                        </div>
                        <div class="col-md-4">
                            @if($dosen->foto == null || $dosen->foto == '')
                            <img src="{{ asset('storage/foto/default.jpg') }}" alt="img-dosen" class="img-fluid img-thumbnail">
                            @else 
                            <img src="{{ asset('storage/foto/'.$dosen->foto) }}" alt="img-dosen" class="img-fluid img-thumbnail">
                            @endif
                        </div>
                    </div>          
                </div>
            </div>
    </div>
</div>

@endsection