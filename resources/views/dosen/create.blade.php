@extends('layouts.app')

@section('title', 'Tambah Dosen')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Tambah Data Dosen</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('dosen.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">       
                        <label>NIDN</label>
                        <input type="number" placeholder="Masukan NIDN" class="form-control" name="nidn" value="{{ old('nidn') }}" required>
                    </div>
                    <div class="form-group">       
                        <label>NIK</label>
                        <input type="number" placeholder="Masukan NIK" class="form-control" name="nik" value="{{ old('nik') }}" required>
                    </div>
                    <div class="form-group">       
                        <label>Kewarganegaraan</label>
                        <select name="kewarganegaraan" class="mySelect form-control">
                            <option value=""> - Pilih Kewarganegaraan - </option>
                            @foreach($countries as $data)
                                <option value="{{ $data->name }}" {{ old('kewarganegaraan') == $data->name ? 'selected' : '' }}>{{ $data->name }}</option>                         
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">       
                        <label>Nama Dosen</label>
                        <input type="text" placeholder="Masukan Nama Dosen" class="form-control" name="nama" value="{{ old('nama') }}" required>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">       
                                <label>Gelar Depan</label>
                                <input type="text" placeholder="contoh: Dr" class="form-control" name="gelar_depan" value="{{ old('gelar_depan') }}">
                            </div>
                        </div>
                        <div class="col-md-6">                            
                            <div class="form-group">
                                <label>Gelar Belakang</label>
                                <input type="text" placeholder="contoh: S.Kom, SH, SE dll" class="form-control" name="gelar_belakang" value="{{ old('gelar_belakang') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">       
                                <label>Tempat Lahir</label>
                                <input type="text" placeholder="Masukan Tempat Lahir" class="form-control" name="tempat_lahir" value="{{ old('tempat_lahir') }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">       
                                <label>Tanggal Lahir</label>
                                <input type="text" placeholder="Masukan Tanggal Lahir" class="form-control datepicker" name="tanggal_lahir" value="{{ old('tanggal_lahir') }}" required>
                            </div>
                        </div>
                    </div>                    
                    <div class="form-group">
                        <label>Jenis Kelamin</label>
                        <select name="jenis_kelamin" class="form-control">
                            <option value=""> - Pilih Jenis Kelamin - </option>
                            <option value="l" {{ old('jenis_kelamin') == 'l' ? 'selected' : '' }}>Laki - Laki</option>
                            <option value="p" {{ old('jenis_kelamin') == 'p' ? 'selected' : '' }}>Perempuan</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Alamat</label>
                        <textarea name="alamat" class="form-control">{{ old('alamat') }}</textarea>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Prodi</label>
                                <select name="prodi_id" class="mySelect form-control ">
                                    <option value=""> - Pilih Prodi - </option>
                                    @foreach($prodi as $data)
                                        <option value="{{ $data->id }}" {{ old('prodi_id') == $data->id ? 'selected' : '' }}>{{ $data->nama_prodi }}</option>                         
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Rumpun Bidang Ilmu</label>
                                <div class="input-group">
                                    <input type="hidden" value="{{ old('bidang_ilmu_id') }}" id="bidang_ilmu_id" name="bidang_ilmu_id">
                                    <input type="text" class="form-control" name="nama_rumpun" value="{{ old('nama_rumpun') }}" id="nama_rumpun" placeholder="Nama Rumpun" readonly>
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-primary" type="button" id="button-addon2" data-toggle="modal" data-target="#rumpun-modal">Pilih</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">       
                        <label>Email</label>
                        <input type="email" placeholder="Masukan Email" class="form-control" name="email" value="{{ old('email') }}" required>
                    </div>

                    <div class="form-group">
                        <label>Agama</label>
                        <select name="agama" class="mySelect form-control">
                            <option value=""> - Pilih Agama - </option>
                            @foreach($agama as $key => $data)
                                <option value="{{ $data }}" {{ old('agama') == $data ? 'selected' : '' }}>{{ $data }}</option>                         
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Tipe Dosen</label>
                        <select name="tipe_dosen" class="mySelect form-control">
                            <option value=""> - Pilih Tipe Dosen - </option>
                            @foreach(getTipeDosen() as $tipe)
                                <option value="{{ $tipe }}" {{ old('tipe_dosen') == $tipe ? 'selected' : '' }}>{{ strtoupper($tipe) }}</option>                         
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Golongan</label>
                        <select name="golongan" class="mySelect form-control">
                            <option value=""> - Pilih Golongan - </option>
                            @foreach(getGolongan() as $golongan)
                                <option value="{{ $golongan }}" {{ old('golongan') == $golongan ? 'selected' : '' }}>{{ strtoupper($golongan) }}</option>                         
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Status</label>
                        <select name="status" class="mySelect form-control">                                    
                            <option value="aktif" {{ old('aktif') == 'aktif' ? 'selected' : '' }}>Aktif</option>
                            <option value="tidak aktif" {{ old('tidak aktif') == 'tidak aktif' ? 'selected' : '' }}>Tidak Aktif</option>                            
                        </select>
                    </div>

                   <div class="form-group">
                       <label>Keterangan</label>
                       <textarea name="keterangan" class="form-control">{{ old('keterangan') }}</textarea>
                   </div>

                   <div class="form-group">
                       <label>Foto</label>
                       <input type="file" name="foto" class="form-control">
                   </div>



                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>                  
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="rumpun-modal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Pilih Rumpun Bidang Ilmu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" id="rumpun-form">
                <div class="modal-body">
                   <div class="row">
                       <div class="col-md-12">
                           <div class="form-group">
    
                               <label>Level 1</label>
                               <select name="level_1" id="level_1" class="mySelect d-block">
                                   <option value="">- Pilih -</option>
                                   @foreach($rumpun as $level_1)
                                       <option value="{{ $level_1->kode }}">{{ $level_1->nama_bidang_ilmu }}</option>
                                   @endforeach
                               </select>
                           </div>
                         
                           <div class="line"></div>
             
                           <label>Level 2</label>
                           <select name="level_2" id="level_2" class="mySelect form-control">
                               <option value="">- Pilih -</option>                    
                           </select>
    
                           <div class="line"></div>
    
                           <label>Level 3</label>
                           <select name="level_3" id="level_3" class="mySelect form-control">
                               <option value="">- Pilih -</option>                    
                           </select>
    
    
                       </div>
                   </div>
                 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="save">Pilih</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection


@push('scripts')

<script>

$(document).ready(function(){
    $('#level_1').change(function(){
        var parent_code = $('#level_1').val();
        $.ajax({
            'type': 'get',
            'url': "{{ url('api/rumpun/level/2') }}",
            'data': {
                'parent_code': parent_code,
                'level': 2,
            },
            success:function(result){
                //remove old element
                $('#level_2 > option').remove();
                $('#level_3 > option').remove();
                //do looping
                $('#level_2').append(new Option(' - Pilih - ', ''));
                $('#level_3').append(new Option(' - Pilih - ', ''));
                $.each(result, function(i, item){
                    //console.log(result[i]['nama_bidang_ilmu']);
                    $('#level_2').append(new Option(result[i]['nama_bidang_ilmu'], result[i]['kode']));
                });                
            },
            error: function(result){
                alert('something went wrong with ajax request.');
            }
        });
    });

    $('#level_2').change(function(){
        var parent_code = $(this).val();
        $.ajax({
            'type': 'get',
            'url': "{{ url('api/rumpun/level/3') }}",
            'data': {
                'parent_code': parent_code,
                'level': 2,
            },
            success:function(result){
                //remove old element
                $('#level_3 > option').remove();
                //do looping
                $('#level_3').append(new Option(' - Pilih - ', ''));
                $.each(result, function(i, item){
                    //console.log(result[i]['nama_bidang_ilmu']);
                    $('#level_3').append(new Option(result[i]['nama_bidang_ilmu'], result[i]['id']));
                });                
            },
            error: function(result){
                alert('something went wrong with ajax request.');
            }
        });
    });

    $('#save').click(function(){
        var nama_rumpun = $('#level_3 :selected').text();
        var value_rumpun = $('#level_3 :selected').val()
        $('#rumpun-modal').modal('hide');

        $('#bidang_ilmu_id').val(value_rumpun);
        $('#nama_rumpun').val(nama_rumpun);
        console.log($('#level_3 :selected').text());
    });
});

</script>

@endpush

