@extends('layouts.app')

@section('title', 'Dashboard')



@section('content')
<div class="row ">  
    <div class="col-lg-4 statistics">
        <!-- Income-->
        <div class="card income text-center" style="padding:50px">
            <div class="icon text-primary"><i class="fa fa-user-tie"></i></div>
            <div class="number">{{ $total_dosen }}</div><strong class="text-primary">Total Dosen</strong>
        </div>
    </div>

    <div class="col-md-4">
        <div class="card p-3">
            <canvas id="barChart" height="246"></canvas>
        </div>
    </div>

    <div class="col-md-4">
        <div class="card p-3 chart-prodi">
            <p class="lead">Jumlah Dosen Pada Prodi </p>
            <hr>
            <canvas id="pieChart"height="215"> </canvas>
        </div>
    </div>

    <div class="col-lg-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Dashboard</h4>
            </div>
            <div class="card-body">
                {{ ucwords("SELAMAT DATANG DI SISTEM PEMETAAN KOMPETENSI DOSEN DI ITB STIKOM BALI. DIAJUKAN SEBAGAI SALAH SATU SYARAT UNTUK MENYUSUN TUGAS AKHIR PROGRAM STUDI S1-SISTEM INFORMASI") }}
            </div>
        </div>
    </div>
</div>



@endsection


@push('scripts')
<script src="{{ asset('templates/vendor/chart.js/Chart.min.js') }}"></script>

<script>

function getDosenRumpun()
{
    return ["{{ $cart_rumpun->jml_dosen[0] }}", "{{ $cart_rumpun->jml_dosen[1] }}", "{{ $cart_rumpun->jml_dosen[2] }}", "{{ $cart_rumpun->jml_dosen[3] }}", "{{ $cart_rumpun->jml_dosen[4] }}"]
}

function getNamaRumpun()
{
    return ["{{ $cart_rumpun->nama_rumpun[0] }}", "{{ $cart_rumpun->nama_rumpun[1] }}", "{{ $cart_rumpun->nama_rumpun[2] }}", "{{ $cart_rumpun->nama_rumpun[3] }}", "{{ $cart_rumpun->nama_rumpun[4] }}"]
}

var ctx = document.getElementById('barChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: getNamaRumpun(),
        datasets: [{
            label: 'Jumlah dosen pada rumpun ilmu',
            data: getDosenRumpun(),
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            xAxes: [{
                ticks: {
                    display: false //this will remove only the label
                }
            }]
        }
    }
});

var dataProdi = {!! $chart_prodi !!};
var namaProdi = [];
var dosenProdi = [];
for(var i = 0; i < dataProdi.length; i++){
    namaProdi.push(dataProdi[i].nama_prodi);
    dosenProdi.push(dataProdi[i].jml_dosen)
}

//console.log(namaProdi);

var brandPrimary = '#379392';

var PIECHART = $('#pieChart');
var myPieChart = new Chart(PIECHART, {
        type: 'doughnut',
        data: {
            labels: namaProdi,
            datasets: [
                {
                    data: dosenProdi,
                    borderWidth: [1, 1, 1, 1],
                    backgroundColor: [
                        brandPrimary,
                        "rgba(67, 209, 105)",
                        "rgba(252, 214, 61)",
                        "rgba(252, 96, 61)"
                    ],
                    hoverBackgroundColor: [
                        brandPrimary,
                        "rgba(67, 209, 105)",
                        "rgba(252, 214, 61)",
                        "rgba(252, 96, 61)"
                    ]
                }]
        }
    });

</script>

@endpush