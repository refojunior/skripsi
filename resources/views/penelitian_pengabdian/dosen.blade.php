@extends('layouts.app')

@section('title')
Penelitian & Pengabdian
@endsection

@section('content')


<div class="row">
    <div class="col-md-12">
        <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h4>Penelitian & Pengabdian </h4>
                </div>
                <div class="card-body">
                    <a class="btn btn-primary" data-toggle="collapse" href="#collapseForm" role="button" aria-expanded="false" aria-controls="collapseForm">
                        Advance Search
                    </a>    
                    <div class="line"></div>
                    <div class="collapse {{ isset($_GET['search']) ? 'show' : '' }}" id="collapseForm">
                        <form action="{{ route('penelitian-pengabdian.index') }}" method="get" class="mb-5">
                            <div class="row">                                        
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nama Dosen</label>
                                        <input type="text" class="form-control" name="name" placeholder="Nama dosen.." value="{{ (isset($_GET['search']) ? $_GET['name'] : '' ) }}">
                                    </div>
                                    <div class="form-group">       
                                        <label>Tipe</label>
                                        <select name="tipe" class="form-control mySelect">
                                            <option value=""> - Pilih Tipe - </option>
                                            @foreach(getTipePP() as $key => $tipe)
                                                <option value="{{ $tipe }}" 
                                                @if(isset($_GET['search']) ? $_GET['name'] : '' )
                                                
                                                {{ $_GET['tipe'] == $tipe ? 'selected' : '' }}
                                                
                                                @endif> {{ $tipe }}</option>                         
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">       
                                        <label>Skim</label>
                                        <select name="skim" class="form-control mySelect">
                                            <option value=""> - Pilih Skim - </option>
                                            @foreach(getSkim() as $key => $skim)
                                                <option value="{{ $skim }}" 
                                                @if(isset($_GET['search']) ? $_GET['name'] : '' )
                                                
                                                {{ $_GET['skim'] == $skim ? 'selected' : '' }}
                                                
                                                @endif> {{ $skim }}</option>                         
                                            @endforeach
                                        </select>
                                    </div>
                                                                       
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Judul Kegiatan</label>
                                        <input type="text" class="form-control" name="judul_kegiatan" value="{{ (isset($_GET['search']) ? $_GET['judul_kegiatan'] : '' ) }}"  placeholder="Judul kegiatan..">
                                    </div>
                                    <div class="form-group">
                                        <label>Tahun</label>
                                        <input type="text" class="form-control" name="tahun_kegiatan" value="{{ (isset($_GET['search']) ? $_GET['tahun_kegiatan'] : '' ) }}" placeholder="Tahun..">
                                    </div>
                                    <div class="form-group">
                                        <label>Lokasi Kegiatan</label>
                                        <input type="text" class="form-control" name="lokasi_kegiatan" value="{{ (isset($_GET['search']) ? $_GET['lokasi_kegiatan'] : '' ) }}" placeholder="lokasi_kegiatan..">
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" value="Cari" name="search" class="btn btn-primary float-right mt-3">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <table class="table table-sm table-striped ">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tipe</th>
                                <th>Skim</th>
                                <th>Dosen</th>
                                <th>Judul Kegiatan</th>
                                <th>Tahun</th>
                                <th>Lokasi</th>
                                <th>Aksi</th>                                                         
                                
                            </tr>       
                        </thead>
                        <tbody>
                            @forelse($penelitian_pengabdian as $row)
                                <tr>
                                    @if(isset($_GET['search']))
                                        <td>{{ $loop->iteration }}</td>
                                    @else
                                        <td>{{ getLoopNumber($penelitian_pengabdian, $loop->index + 1) }}</td>
                                    @endif
                                    <td>{{ ucwords($row->tipe) }}</td>
                                    <td>{{ $row->skim }}</td>
                                    @if(isset($_GET['search']))
                                        <td>{{ getDosenName($row->dosen_id) }}</td>
                                    @else
                                        <td>{{ $row->dosen->nama }}</td>
                                    @endif
                                    <td>{{ $row->judul_kegiatan }}</td>
                                    <td>{{ $row->tahun_kegiatan }}</td>
                                    <td>{{ $row->lokasi_kegiatan }}</td> 
                                    <td>
                                        <ul class="d-flex action-button">                                            
                                            <li><a href="{{ route('penelitian-pengabdian.indexDosen', $row->dosen_id) }}" class="text-primary" title="Lihat Detail Dosen"><i class="fa fa-user-tie"></i></a></li>                                            
                                        </ul>
                                    </td>
                                    
                                </tr>
                            @empty 
                        </tbody>
                       
                        <tr>
                            <td colspan="9">Data penelitian pengabdian tidak ada.</td>
                        </tr>
                        @endforelse
                        
                        
                    </table>
                    @if(!isset($_GET['search']))
                        {{ $penelitian_pengabdian->links() }}
                    @endif
                </div>
        </div>
    </div>
</div>



@endsection