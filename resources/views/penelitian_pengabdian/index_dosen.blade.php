@extends('layouts.app')

@section('title')
Data Penelitian & Pengabdian {{ $dosen->namaGelar() }}
@endsection

@section('content')


<div class="row">
    <div class="col-md-12">
        @include('dosen.tabs')
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h4>Penelitian & Pengabdian {{ $dosen->namaGelar() }}</h4>
                </div>
                <div class="card-body">
                <a href="{{ route('penelitian-pengabdian.create', $dosen->id) }}" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Tambah Penelitian & Pengabdian</a>
                    <table class="table diklat-table">
                        <tr>
                            <th>No</th>
                            <th>Tipe</th>
                            <th>SKIM</th>
                            <th>Judul Kegiatan</th>
                            <th>Tahun</th>
                            <th>Lokasi</th>
                            <th>Durasi</th>                                                       
                            <th>Aksi</th>
                        </tr>       
                       
                        @forelse($penelitian_pengabdian as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->tipe }}</td>
                            <td>{{ $row->skim }}</td>
                            <td>{{ $row->judul_kegiatan }}</td>
                            <td>{{ $row->tahun_kegiatan }}</td>
                            <td>{{ $row->lokasi_kegiatan }}</td> 
                            <td>{{ $row->lama_kegiatan }} Tahun</td>                             
                            <td>
                                <form action="{{ route('penelitian-pengabdian.destroy', $row->id) }}" method="post">
                                    <ul class="d-flex action-button">
                                        @csrf
                                        @method('delete')
                                        <li><a href="{{ route('penelitian-pengabdian.edit', $row->id) }}" class="text-secondary" title="Edit"><i class="fa fa-edit"></i></a></li>
                                        <!-- <li><button type="submit" class="text-danger btn-submit" onclick="return confirm('Yakin akan hapus data?')" ><i class="fa fa-trash"></i></button></li> -->
                                    </ul>
                                </form>
                            </td>
                        </tr>
                        @empty 
                        <tr>
                            <td colspan="11">Dosen ini belum memiliki penelitian & pengabdian.</td>
                        </tr>
                        @endforelse
                        
                        
                    </table>
                </div>
        </div>
    </div>
</div>



@endsection