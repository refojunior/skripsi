@extends('layouts.app')

@section('title')
Edit Penelitian & Pengabdian 
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Edit Penelitian & Pengabdian</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('penelitian-pengabdian.update', $penelitian_pengabdian->id) }}" method="post" class="form-horizontal">
                    @csrf    
                    @method('put')               
                    <div class="form-group row">       
                        <label class="col-md-2 form-control-label">Dosen</label>
                        <div class="col-md-10">
                            <select name="dosen_id" class="form-control" readonly>
                                <option value="{{ $penelitian_pengabdian->dosen->id }}"> {{ $penelitian_pengabdian->dosen->namaGelar() }} </option>                               
                            </select>
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">       
                        <label class="col-sm-2 form-control-label">Tipe Kegiatan</label>
                        <div class="col-sm-10">
                            <select name="tipe" class="form-control mySelect">
                                <option value=""> - Pilih Tipe - </option>
                                <option value="penelitian" {{ $penelitian_pengabdian->tipe == 'penelitian' ? 'selected' : '' }} >Penelitian</option>
                                <option value="pengabdian" {{ $penelitian_pengabdian->tipe == 'pengabdian' ? 'selected' : '' }} >Pengabdian</option>
                            </select>
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">       
                        <label class="col-sm-2 form-control-label">SKIM</label>
                        <div class="col-sm-10">
                            <select name="skim" class="form-control mySelect">
                                <option value=""> - Pilih Skim - </option>
                                @foreach(getSkim() as $skim)
                                <option value="{{ $skim }}" {{ $penelitian_pengabdian->skim == $skim ? 'selected' : '' }} >{{ $skim }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">       
                        <label class="col-sm-2 form-control-label">Judul Kegiatan</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="judul_kegiatan" value="{{ $penelitian_pengabdian->judul_kegiatan }}" placeholder="Masukan judul kegiatan">
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">       
                        <label class="col-sm-2 form-control-label">Tahun Kegiatan</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="tahun_kegiatan" value="{{ $penelitian_pengabdian->tahun_kegiatan }}" placeholder="contoh: 2010, 2011, 2012 dst">
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Lokasi Kegiatan</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="lokasi_kegiatan" value="{{ $penelitian_pengabdian->lokasi_kegiatan }}" placeholder="Masukan lokasi kegiatan">
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Lama Kegiatan</label>
                        <div class="col-sm-10 input-group">
                            <input type="text" class="form-control" name="lama_kegiatan" value="{{ $penelitian_pengabdian->lama_kegiatan }}" placeholder="contoh: 1, 2, 3">
                            <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon2">Tahun</span>
                            </div>
                        </div>
                    </div>
                    <div class="line"></div>
                    
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>                  
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script>
    var cleave = new Cleave('.thousand-input2', {
          numeral: true,
          numeralThousandsGroupStyle: 'thousand'
        });
        var cleave = new Cleave('.thousand-input3', {
          numeral: true,
          numeralThousandsGroupStyle: 'thousand'
        });
</script>
@endpush