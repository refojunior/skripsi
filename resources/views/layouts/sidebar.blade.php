<!-- Side Navbar -->
<nav class="side-navbar">
    <div class="side-navbar-wrapper">
        <!-- Sidebar Header    -->
        <div class="sidenav-header d-flex align-items-center justify-content-center">
            <!-- User Info-->
            <div class="sidenav-header-inner text-center">
            <h2 class="h5">{{ Auth::user()->username }}</h2><span class="text-secondary">{{ Auth::user()->getLevel() }}</span>
            </div>
            <!-- Small Brand information, appears on minimized sidebar-->
            <div class="sidenav-header-logo"><a href="index.html" class="brand-small text-center"> <strong>S</strong><strong class="text-primary">I</strong></a></div>
        </div>
        <!-- Sidebar Navigation Menus-->
        <div class="main-menu">
            <h5 class="sidenav-heading">Main</h5>
            <ul id="side-main-menu" class="side-menu list-unstyled">                  
                <li class="{{ (urlHasPrefix('dashboard') == true ) ? 'active' : '' }}"><a href="{{ url('dashboard') }}"> <i class="icon-home"></i>Dashboard</a></li>
                <li class="{{ (urlHasPrefix('prodi') == true ) ? 'active' : '' }}"><a href="{{ route('prodi.index') }}"> <i class="icon-form"></i>Program Studi</a></li>
                <li class="{{ (urlHasPrefix('bidang-ilmu') == true ) ? 'active' : '' }}"><a href="{{ route('bidang-ilmu.index') }}"> <i class="icon-flask"></i>Rumpun Ilmu</a></li>
                <li class="{{ (urlHasPrefix('kompetensi') == true ) ? 'active' : '' }}"><a href="{{ route('kompetensi.index') }}"> <i class="fa fa-suitcase"></i>Kompetensi</a></li>
                <li class="{{ (urlHasPrefix('almamater') == true ) ? 'active' : '' }}"><a href="{{ route('almamater.index') }}"> <i class="fa fa-school"></i>Almamater</a></li>
                <li class="{{ (urlHasPrefix('dosen') == true ) ? 'active' : '' }}"><a href="{{ route('dosen.index') }}"> <i class="fa fa-user-tie"></i>Dosen</a></li>
                <li class="{{ (urlHasPrefix('user') == true ) ? 'active' : '' }}"><a href="{{ route('user.index') }}"> <i class="fa fa-users"></i>User & Kaprodi</a></li>
            </ul>
        <div class="admin-menu mt-4">
            <h5 class="sidenav-heading">Cari</h5>
            <ul id="side-admin-menu" class="side-menu list-unstyled"> 
                <li class="{{ (urlHasPrefix('jabatan-fungsional') == true ) ? 'active' : '' }}"><a href="{{ route('jabatan-fungsional.index') }}"> <i class="fa fa-clipboard-list"></i>Jabatan Fungsional</a></li>
                <li class="{{ (urlHasPrefix('diklat') == true ) ? 'active' : '' }}"><a href="{{ route('diklat.index') }}"> <i class="fa fa-archive"></i>Diklat</a></li>
                <li class="{{ (urlHasPrefix('penelitian-pengabdian') == true ) ? 'active' : '' }}"><a href="{{ route('penelitian-pengabdian.index') }}"> <i class="fa fa-hands"></i>Penelitian Pengabdian</a></li>
                <li class="{{ (urlHasPrefix('sertifikasi') == true ) ? 'active' : '' }}"><a href="{{ route('sertifikasi') }}"> <i class="fa fa-book-open"></i>Sertifikasi</a></li>
                <li class="{{ (urlHasPrefix('pengajaran') == true ) ? 'active' : '' }}"> <a href="{{ route('pengajaran') }}"> <i class="fa fa-book"> </i>Pengajaran</a></li>            
            </ul>
        </div>
    </div>
</nav>