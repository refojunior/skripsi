@extends('layouts.app')

@section('title')
Pengajaran
@endsection

@section('content')


<div class="row">
    <div class="col-md-12">
        <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h4>Pengajaran </h4>
                </div>
                <div class="card-body">
                    <a class="btn btn-primary" data-toggle="collapse" href="#collapseForm" role="button" aria-expanded="false" aria-controls="collapseForm">
                        Advance Search
                    </a>    
                    <div class="line"></div>
                    <div class="collapse {{ isset($_GET['search']) ? 'show' : '' }}" id="collapseForm">
                        <form action="{{ route('pengajaran') }}" method="get" class="mb-5">
                            <div class="row">                                        
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nama Dosen</label>
                                        <input type="text" class="form-control" name="name" placeholder="Nama dosen.." value="{{ (isset($_GET['search']) ? $_GET['name'] : '' ) }}">
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Kode Kelas</label>
                                        <input type="text" class="form-control" name="kode_kelas" value="{{ (isset($_GET['search']) ? $_GET['kode_kelas'] : '' ) }}" placeholder="Kode kelas..">
                                    </div>
                                    <div class="form-group">
                                        <label>Matakuliah</label>
                                        <input type="text" class="form-control" name="matakuliah" value="{{ (isset($_GET['search']) ? $_GET['matakuliah'] : '' ) }}" placeholder="Matakuliah..">
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">       
                                        <label>Semester</label>
                                        <select name="semester" class="form-control mySelect">
                                            <option value=""> - Pilih semester - </option>
                                            @foreach(getSemester() as $key => $semester)
                                                <option value="{{ $semester }}" 
                                                @if(isset($_GET['search']) ? $_GET['semester'] : '' )
                                                
                                                {{ $_GET['semester'] == $semester ? 'selected' : '' }}
                                                
                                                @endif> {{ $semester }}</option>                         
                                            @endforeach
                                        </select>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Tahun Ajar</label>
                                        <input type="text" class="form-control" name="tahun_ajar" value="{{ (isset($_GET['search']) ? $_GET['tahun_ajar'] : '' ) }}" placeholder="Nomor peserta..">
                                    </div>                                    
                                    <div class="form-group">
                                        <input type="submit" value="Cari" name="search" class="btn btn-primary float-right mt-3">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <table class="table table-sm table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Dosen</th>
                                <th>Kode Kelas</th>
                                <th>Semester</th>
                                <th>Tahun Ajar</th>                                
                                <th>File</th>                           
                                <th>Aksi</th>
                            </tr>   
                        </thead>
                        <tbody>
                            @foreach($pengajaran as $row)
                            <tr>                                
                                @if(isset($_GET['search']))
                                    <td>{{ $loop->iteration }}</td>
                                @else
                                    <td>{{ getLoopNumber($pengajaran, $loop->index + 1) }}</td>
                                @endif
                                @if(isset($_GET['search']))
                                    <td>{{ getDosenName($row->dosen_id) }}</td>
                                @else
                                    <td>{{ $row->dosen->nama }}</td>
                                @endif
                                <td>{{ $row->kode_kelas }}</td>
                                <td>{{ $row->matakuliah }}</td>
                                <td>{{ $row->semester }}</td>
                                <td>{{ $row->tahun_ajar }}</td>                              
                                <td>                                    
                                    <ul class="d-flex action-button">                                            
                                        <li><a href="{{ route('pengajaran.index', [$row->dosen_id]) }}" class="text-primary" title="Lihat Detail Dosen"><i class="fa fa-user-tie"></i></a></li>                                            
                                    </ul>

                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    @if(!isset($_GET['search']))
                    {{ $pengajaran->links() }}
                    @endif
                </div>
        </div>
    </div>
</div>

@endsection