@extends('layouts.app')

@section('title')
Dosen Almamater {{ $almamater->nama_almamater }}
@endsection


@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Data Dosen Pada Almamater {{ $almamater->nama_almamater }}</h4>
            </div>
            <div class="card-body mt-3">                
                <table class="table table-sm table-striped datatable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>NIDN</th>
                            <th>Nama</th>
                            <th>Bidang Studi</th>
                            <th>Gelar</th>
                            <th>Tahun Lulus</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($pendidikan as $row)                        
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->dosen->nidn }}</td>
                            <td>{{ $row->dosen->nama }}</td>
                            <td>{{ $row->bidang_studi }}</td>
                            <td>{{ $row->gelar }}</td>
                            <td>{{ $row->tahun_lulus }}</td>
                            <td>
                                <a href="{{ route('pendidikan.index', $row->dosen_id) }}" class="text-info" title="Lihat Info Dosen"><i class="fa fa-address-card"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection