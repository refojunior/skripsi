@extends('layouts.app')

@section('title', 'Almamater')

@section('content')


<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Data Almamater Dosen</h4>
            </div>
            <div class="card-body">
                <a href="{{ route('almamater.create') }}" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Tambah Almamater</a>
                <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Almamater</th>
                            <th>Alamat</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($almamater as $row)
                       <tr>
                           <td>{{ $loop->iteration }}</td>
                           <td>{{ $row->nama_almamater }}</td>
                           <td>{{ $row->alamat }}</td>
                           <td>
                                <form action="{{ route('almamater.destroy', $row->id) }}" method="post">
                                    <ul class="d-flex action-button">
                                        <li><a href="{{ route('almamater.dosen', $row->id) }}" class="text-primary" title="Lihat dosen dengan almamater ini"><i class="fa fa-user-tie"></i></a></li>
                                        <li><a href="{{ route('almamater.edit', $row->id) }}" class="text-secondary" title="Edit"><i class="fa fa-edit"></i></a></li>
                                        @csrf
                                        @method('delete')
                                        <!-- <li><button type="submit" class="text-danger btn-submit" onclick="return confirm('Yakin akan hapus data?')" ><i class="fa fa-trash"></i></button></li> -->
                                    </ul>
                                </form>
                           </td>
                       </tr>
                       @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection