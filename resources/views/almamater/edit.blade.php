@extends('layouts.app')

@section('title', 'Edit Almamater')

@section('content')

<div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Edit Almamater</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('almamater.update', $almamater->id) }}" method="post">
                    @csrf
                    @method('put')
                    <div class="form-group">       
                        <label>Nama Almamater</label>
                        <input type="text" placeholder="contoh: Institut Teknologi dan Bisnis (ITB) STIKOM Bali" class="form-control" name="nama_almamater" value="{{ $almamater->nama_almamater }}">
                    </div>  
                    <div class="form-group">
                        <label>Alamat</label>
                        <textarea name="alamat" class="form-control" placeholder="Masukan Alamat">{{ $almamater->alamat }}</textarea>
                    </div>                    
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>                  
                </form>
            </div>
        </div>
    </div>
</div>

@endsection