@extends('layouts.app')

@section('title', 'Tambah Almamater')

@section('content')

<div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Tambah Almamater</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('almamater.store') }}" method="post">
                    @csrf
                    <div class="form-group">       
                        <label>Nama Almamater</label>
                        <input type="text" placeholder="contoh: Institut Teknologi dan Bisnis (ITB) STIKOM Bali" class="form-control" name="nama_almamater" value="{{ old('nama_almamater') }}">
                    </div>  
                    <div class="form-group">
                        <label>Alamat</label>
                        <textarea name="alamat" class="form-control" placeholder="Masukan Alamat">{{ old('alamat') }}</textarea>
                    </div>                    
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>                  
                </form>
            </div>
        </div>
    </div>
</div>

@endsection