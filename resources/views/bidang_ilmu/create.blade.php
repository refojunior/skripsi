@extends('layouts.app')

@section('title', 'Tambah Bidang Ilmu')

@section('content')

<div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Tambah Rumpun Bidang Ilmu</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('bidang-ilmu.store') }}" method="post">
                    @csrf
                    <div class="form-group">       
                        <label>Nama Bidang Ilmu</label>
                        <input type="text" placeholder="Masukan Nama Bidang Ilmu" class="form-control" name="nama_bidang_ilmu" value="{{ old('nama_bidang_ilmu') }}">
                    </div>  
                    <div class="form-group">
                        <label>Deskripsi</label>
                        <textarea name="deskripsi" class="form-control" placeholder="Masukan Deskripsi">{{ old('deskripsi') }}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Level</label>
                        <select name="level" class="form-control">
                            <option value=""> - Pilih Level - </option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>                  
                </form>
            </div>
        </div>
    </div>
</div>

@endsection