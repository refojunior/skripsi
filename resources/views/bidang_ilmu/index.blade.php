@extends('layouts.app')

@section('title', 'Program Studi')

@section('content')


<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Data Rumpun Bidang Ilmu</h4>
            </div>
            <div class="card-body">

                <div class="row mb-3">
                    <div class="col-md-7">
                        <div class="form-group">
                            <select name="rumpun" id="show_rumpun" class="form-control">
                                <option value="1">Tampilkan seluruh rumpun bidang ilmu</option>
                                <option value="0" {{ (isset($_GET['type'])) ? 'selected' : '' }} >Tampilkan rumpun bidang ilmu yang dimiliki oleh dosen di STIKOM Bali</option>
                            </select>
                        </div>
                    </div>
                </div>
                
                <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode</th>
                            <th>Nama Bidang Ilmu</th>                
                            <th class="text-center">Level</th>                       
                            <th class="text-center">Dosen</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($bidang_ilmu as $row)
                       <tr class="{{ isRumpunLevel($row->level) }} ">
                           <td>{{ $loop->iteration }}</td>
                           <td>{{ $row->kode }}</td>
                           <td>{{ $row->nama_bidang_ilmu }}</td>                   
                           <td class="text-center">{{ $row->level }}</td>
                           <td class="text-center">
                               @if($row->level != 3)
                                -
                               @else 
                                
                                <a href="{{ route('bidang-ilmu.dosen', $row->id) }}" class="text-primary" title="Lihat dosen bidang ilmu ini"><i class="fa fa-user-tie"></i></a>
                               
                                @endif
                           </td>
                       </tr>
                       @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection


@push('scripts')

<script>

$(document).ready(function(){
    $('#show_rumpun').change(function(){
        if($('#show_rumpun').val() == 1){
            window.location = "{{ route('bidang-ilmu.index') }}";
        } else {
            window.location = "{{ route('bidang-ilmu.index', ['type' => 'all' ]) }}";
        }
    });
});

</script>

@endpush