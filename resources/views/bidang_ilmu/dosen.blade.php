@extends('layouts.app')

@section('title')
Dosen Bidang Ilmu {{ $bidang_ilmu->nama_bidang_ilmu }}
@endsection


@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Data Dosen Pada Bidang Ilmu {{ $bidang_ilmu->nama_bidang_ilmu }}</h4>
            </div>
            <div class="card-body mt-3">                
                <table class="table table-sm table-striped datatable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>NIDN</th>
                            <th>Nama</th>
                            <th>Bidang Ilmu</th>
                            <th>Prodi</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($dosen as $row)                        
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->nidn }}</td>
                            <td>{{ $row->nama }}</td>
                            <td>{{ $row->bidangIlmu->nama_bidang_ilmu }}</td>
                            <td>{{ $row->prodi->nama_prodi }}</td>
                            <td>
                                <a href="{{ route('dosen.show', $row->id) }}" class="text-info" title="Lihat Info Dosen"><i class="fa fa-address-card"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection