<style>

* {
    font-family: "Arial";
}
/* .print-header {
    display: flex;
    align-items: center;
    font-family: "Arial";
} */

.print-header .left,
.print-header .right {
    display:inline-block;
}
.print-header p{
    line-height:1.1em;
    font-family: "Arial";
}

.img-box {
    padding: 0 40px;
}

table {
    width:100%;
}

table {
  border-collapse: collapse;
}

table, th, td {
  border: 1px solid black;
}

</style>

<div class="container">
    <div class="print-header">
        <div class="left">
            <div class="img-box">
                
                <img src="<?php echo public_path() . '/img/logo-stikom.png' ?>" alt="logo_stikom" width="100">
            </div>
        </div>
        <div class="right">
            <h4>INSTITUT TEKNOLOGI DAN BISNIS STIKOM BALI</h4>
            <p>Jl. Raya Puputan No. 86 Renon, Denpasar</p>
            <p>Telp. (0361) 244445 Fax (0361) 265773</p>
        </div>
    </div>
    <hr>
    <div class="print-content">
        <h4>Data Dosen di ITB STIKOM Bali</h4>
    </div>
    <table border="1">
        <thead>
            <tr>
                <th>No</th>
                <th>NIDN</th>
                <th>Nama Dosen</th>                           
                <th>Prodi</th>
                <th>Tipe</th>
                <th>Status</th>             
            </tr>
        </thead>
        <tbody>
            @foreach($dosen as $row)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $row->nidn }}</td>
                <td>{{ $row->nama }}</td>                            
                <td>{{ $row->prodi->nama_prodi }}</td>
                <td>{{ strtoupper($row->tipe_dosen) }}</td>
                <td class="text-center">{!! $row->getStatus() !!}</td>
                
            </tr>
            @endforeach
        </tbody>
    </table>
</div>