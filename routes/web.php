<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});



Auth::routes();
Route::middleware('auth')->group(function(){
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');
    Route::resource('prodi', 'ProdiController');
    Route::resource('bidang-ilmu', 'BidangIlmuController');
    Route::resource('dosen', 'DosenController');
    Route::resource('kompetensi', 'KompetensiController');
    Route::resource('almamater', 'AlmamaterController');
    Route::resource('user', 'UserController');

    // kompetensi dosen
    Route::get('dosen/{id}/kompetensi-dosen', 'KompetensiDosenController@index')->name('kompetensi-dosen.index');
    Route::post('dosen/{id}/kompetensi-dosen', 'KompetensiDosenController@store')->name('kompetensi-dosen.store');
    Route::delete('dosen/{id}/kompetensi-dosen/{dosen_id}', 'KompetensiDosenController@delete')->name('kompetensi-dosen.destroy');
    //end kompetensi dosen

    //pendidikan dosen
    Route::get('dosen/{id}/pendidikan', 'PendidikanController@index')->name('pendidikan.index');
    Route::get('dosen/{id}/pendidikan/create', 'PendidikanController@create')->name('pendidikan.create');
    Route::post('dosen/{id}/pendidikan/create', 'PendidikanController@store')->name('pendidikan.store');
    Route::delete('dosen/{id}/pendidikan', 'PendidikanController@delete')->name('pendidikan.destroy');
    Route::get('pendidikan/{penidikan_id}/edit', 'PendidikanController@edit')->name('pendidikan.edit');
    Route::put('pendidikan/{pendidikan_id}/update', 'PendidikanController@update')->name('pendidikan.update');
    //end pendidikan dosen

    //jabatan fungsional
    Route::get('dosen/{id}/jabatan-fungsional', 'JabatanFungsionalController@indexDosen')->name('jabatan-fungsional.indexDosen');
    Route::get('dosen/{id}/jabatan-fungsional/create', 'JabatanFungsionalController@create')->name('jabatan-fungsional.create');
    Route::post('jabatan-fungsional/store', 'JabatanFungsionalController@store')->name('jabatan-fungsional.store');
    Route::get('jabatan-fungsional/{jabatan_id}/edit', 'JabatanFungsionalController@edit')->name('jabatan-fungsional.edit');
    Route::put('jabatan-fungsional/{jabatan_id}/update', 'JabatanFungsionalController@update')->name('jabatan-fungsional.update');
    Route::delete('jabatan-fungsional/{id}/delete', 'JabatanFungsionalController@delete')->name('jabatan-fungsional.destroy');
    Route::get('jabatan-fungsional', 'JabatanFungsionalController@index')->name('jabatan-fungsional.index');
    //end jabatan fungsional 

    //diklat
    Route::get('dosen/{dosen_id}/diklat', 'DiklatController@indexDosen')->name('diklat.indexDosen');
    Route::get('dosen/{dosen_id}/diklat/create', 'DiklatController@create')->name('diklat.create');
    Route::post('diklat/store', 'DiklatController@store')->name('diklat.store');
    Route::delete('diklat/{id}/delete', 'DiklatController@destroy')->name('diklat.destroy');
    Route::get('diklat/{id}/edit', 'DiklatController@edit')->name('diklat.edit');
    Route::PUT('diklat/{id}/update', 'DiklatController@update')->name('diklat.update');
    Route::get('diklat', 'DiklatController@index')->name('diklat.index');
    //end diklat

    //Penelitian pengabdian
    Route::get('dosen/{dosen_id}/penelitian-pengabdian', 'PenelitianPengabdianController@indexDosen')->name('penelitian-pengabdian.indexDosen');
    Route::get('dosen/{dosen_id}/penelitian-pengabdian/create', 'PenelitianPengabdianController@create')->name('penelitian-pengabdian.create');
    Route::post('penelitian-pengabdian/store', 'PenelitianPengabdianController@store')->name('penelitian-pengabdian.store');
    Route::delete('penelitian-pengabdian/{id}/delete', 'PenelitianPengabdianController@destroy')->name('penelitian-pengabdian.destroy');
    Route::get('penelitian-pengabdian/{id}/edit', 'PenelitianPengabdianController@edit')->name('penelitian-pengabdian.edit');
    Route::PUT('penelitian-pengabdian/{id}/update', 'PenelitianPengabdianController@update')->name('penelitian-pengabdian.update');
    Route::get('penelitian-pengabdian', 'PenelitianPengabdianController@index')->name('penelitian-pengabdian.index');
    //end penelitian pengabdian


    //pengajaran
    Route::resource('dosen/{dosen_id}/pengajaran', 'PengajaranController');
    Route::get('pengajaran', 'PengajaranController@indexAll')->name('pengajaran');
    //end pengajaran

    //sertifikasi
    Route::resource('dosen/{dosen_id}/sertifikasi', 'SertifikasiController');
    Route::get('sertifikasi', 'SertifikasiController@indexAll')->name('sertifikasi');
    //endsertifikasi

    //dosen prodi
    Route::get('prodi/{prodi_id}/dosen', 'ProdiController@dosen')->name('prodi.dosen');
    //end dosen prodi

    //dosen bidang ilmu
    Route::get('bidang-ilmu/{bidang_id}/dosen', 'BidangIlmuController@dosen')->name('bidang-ilmu.dosen');
    //end bidang ilmu

     //dosen prodi
     Route::get('almamater/{almamater_id}/dosen', 'AlmamaterController@dosen')->name('almamater.dosen');
     //end dosen prodi

     //dosen prodi
     Route::get('kompetensi/{kompetensi_id}/dosen', 'KompetensiController@dosen')->name('kompetensi.dosen');
     //end dosen prodi


     //print pdf
     Route::get('print-dosen', 'PdfController@dosen')->name('dosen.print');
     Route::get('print-dosen-prodi/{id}', 'PdfController@dosenProdi')->name('print.dosen-prodi');
     //excell
     Route::get('export-dosen', 'ExcelController@exportDosen')->name('export.dosen');
     Route::get('export-dosen-prodi/{id}', 'ExcelController@exportDosenProdi')->name('export.dosen-prodi');
});

