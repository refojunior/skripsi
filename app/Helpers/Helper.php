<?php 
use App\Dosen;

function urlHasPrefix(string $prefix) {
    $url = url()->current();
    if (strpos($url, $prefix) > 0) {
        return true;
    }

    return false;
}


function generateBreadcrumb(){
    
    $crumbs = explode("/",$_SERVER["REQUEST_URI"]);
    //dd((is_numeric($crumbs[2]) == true) ? 'benar' : 'salah');
    foreach($crumbs as $key=>$crumb){
        
        if($key == 0 || is_numeric($crumbs[$key])) {
            continue;
        } else {      
            if($key == count($crumbs) - 1) {
                $breadcrumb = $crumbs[$key];
                if(strpos($breadcrumb, '?') !== false){
                    $breadcrumb = explode("?", $breadcrumb);
                    $crumb = $breadcrumb[0];
                }
              
                echo  '<li class="breadcrumb-item active">' . ucfirst(str_replace('-', ' ',$crumb) . ' ') . '</li>';
            } else {
                echo '<li class="breadcrumb-item"><a href="'.url($crumb).'">' . ucfirst(str_replace('-', ' ',$crumb) . ' ') . '</a></li>';
            }
            
        }
        
    }
}

function getJabatanFungsional(){
    return [
        'Asisten Ahli (100.00)',
        'Asisten Ahli (150.00)',
        'Lektor (200.00)',
        'Lektor (300.00)',
        'Lektor Kepala (400.00)',
        'Lektor Kepala (550.00)',
        'Lektor Kepala (700.00)',
        'Profesor (850.00)',
        'Profesor (1050.00)'
    ];
}

function getTingkat() {
    return [
        'lokal',
        'regional',
        'nasional',
        'internasional'
    ];
}


function getTipeDosen(){
    return [
        'homebase',
        'dpk',
        'lb',
        'pns'
    ];
}


function getGolongan(){
    return [
        'III/A', 'III/B', 'III/C', 'III/D', 'IV/A', 'IV/B', 'IV/C', 'IV/E'
    ];
}

function getSkim(){
    return [
        'Biomedik',
        'Hibah HI-LINK',
        'Ipteks',
        'Ipteks Bagi Inovasi Kreativitas Kampus',
        'Ipteks Bagi Kewirausahaan',
        'Ipteks Bagi Masyarakat',
        'Ipteks Bagi Produk Ekspor',
        'Ipteks Bagi Wilayah',
        'Ipteks Bagi Wilayah Antara PT-CSR / PT-PEMDA-CSR',
        'Kerjasama Luar Negeri dan Publikasi Internasional',
        'KKN Pembelajaran Pemberdayaan Masyarakat',
        'Mobil Listrik Nasional',
        'MP3EI',
        'Pendidikan Magister Doktor Sarjana Unggul',
        'Penelitian Dana Lokal Perguruan Tinggi',
        'Penelitian Disertasi Doktor',
        'Penelitian Dosen Pemula',
        'Penelitian Fundamental',
        'Penelitian Hibah Bersaing',
        'Penelitian Kerjasama Antar Perguruan Tinggi',
        'Penelitian Kompetensi',
        'Penelitian Strategis Nasional',
        'Penelitian Tim Pasca Sarjana',
        'Penelitian Unggulan Perguruan Tinggi',
        'Penelitian Unggulan Strategis Nasional',
        'Riset Andalan Perguruan Tinggi dan Industri'
    ];
}

function getTipePP() {
    return [
        'penelitian',
        'pengabdian'
    ];
}

function getSemester(){
    return [
        'ganjil',
        'genap'
    ];
}

function getJenjangPendidikan(){
    return [
        'Diploma 1 / D1',
        'Diploma 2 / D2',
        'Diploma 3 / D3',
        'Diploma 4 / D4',
        'Strata 1 / Sarjana',
        'Strata 2 / Magister',
        'Strata 3 / Doktor'
    ];
}

function isRumpunLevel($level){
    if($level == 1) {
        return 'text-danger font-weight-bold';
    } else if($level == 2) {
        return 'text-info font-weight-bold';
    }
}

function getLoopNumber($items, $loop){
    return ($items->currentpage()-1) * $items->perpage() + $loop;
}


function getDosenName($dosen_id) {
    return Dosen::findOrFail($dosen_id)->nama;
}

?>