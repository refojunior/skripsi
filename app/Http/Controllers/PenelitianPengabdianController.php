<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dosen;
use App\Http\Requests\StorePenelitianPengabdian;
use App\PenelitianPengabdian;

class PenelitianPengabdianController extends Controller
{
    public function indexDosen($dosen_id)
    {
        $data['dosen'] = Dosen::find($dosen_id);
        $data['penelitian_pengabdian'] = $data['dosen']->penelitianPengabdian;
        $data['tab'] = 6;

        return view('penelitian_pengabdian.index_dosen', $data);
    }

    public function create($dosen_id)
    {
        $dosen = Dosen::find($dosen_id);
        
        return view('penelitian_pengabdian.create')->with(['dosen' => $dosen]);
    }

    public function store(StorePenelitianPengabdian $request)
    {
        
        PenelitianPengabdian::create($request->all());

        return redirect()->route('penelitian-pengabdian.indexDosen', $request->dosen_id)->with('success', 'Berhasil menambah diklat dosen');;
        

    }

    public function edit($id)
    {
    
        $data['penelitian_pengabdian'] = PenelitianPengabdian::findOrFail($id);
    
        return view ('penelitian_pengabdian.edit', $data);
    }

    public function update(StorePenelitianPengabdian $request, $id)
    {        
        PenelitianPengabdian::findOrFail($id)->update($request->all());
        return redirect()->route('penelitian-pengabdian.indexDosen', $request->dosen_id)->with('success', 'Berhasil mengubah data penelitian atau pengabdian dosen');   
    }

    public function destroy($id) 
    {
        PenelitianPengabdian::findOrFail($id)->delete();
        return redirect()->back()->with('success', 'Berhasil menghapus data penelitian atau pengabdian dosen');
        
    }

    public function index(Request $request)
    {
        $pp = new PenelitianPengabdian();
        $data['penelitian_pengabdian'] = $pp->paginate(15);
        if(isset($request->search)){
            $data['penelitian_pengabdian'] = $pp->advanceSearch($request->all());
        }
        return view('penelitian_pengabdian.dosen', $data);
    }
}
