<?php

namespace App\Http\Controllers;

use App\BidangIlmu;
use App\Dosen;
use App\Http\Requests\StoreDosen;
use App\Prodi;
use Illuminate\Http\Request;

class DosenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('kaprodi', ['only' => ['create', 'edit', 'destroy']]);
    }

    public function index()
    {
        return view('dosen.index')->with('dosen', Dosen::orderBy('nama', 'asc')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['countries'] = json_decode(file_get_contents('https://restcountries.eu/rest/v2/all'));
        $data['agama'] = ['Islam', 'Kristen', 'Hindu', 'Buddha', 'Konghucu'];
        $data['prodi'] = Prodi::all();     
        $data['rumpun'] = BidangIlmu::where('level', 1)->get();
        return view('dosen.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDosen $request)
    {
        
        if($request->hasFile('foto')){
            $format = $request->foto->extension();
            $rename = 'foto_'.strtotime("now").'.'.$format;

            $request->foto->storeAs('foto', $rename);
        } else {
            $rename = null;
        }
        $input = $request->all();
        $input['foto'] = $rename;

        Dosen::create($input);
        return redirect()->route('dosen.index')->with('success', 'Berhasil menambah data dosen');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['dosen'] = Dosen::findOrFail($id);
        $data['tab'] = 1;
        return view('dosen.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['dosen'] = Dosen::find($id);
        $data['countries'] = json_decode(file_get_contents('https://restcountries.eu/rest/v2/all'));
        $data['agama'] = ['Islam', 'Kristen', 'Hindu', 'Buddha', 'Konghucu'];
        $data['prodi'] = Prodi::all();
        $data['rumpun'] = BidangIlmu::where('level', 1)->get();
        return view('dosen.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreDosen $request, $id)
    {
        $dosen = Dosen::find($id);

        if($request->hasFile('foto')){
            //remove old photos
            if(is_file('storage/foto/'.$dosen->foto)){
                unlink('storage/foto/'.$dosen->foto);
            }

            $format = $request->foto->extension();
            $rename = 'foto_'.strtotime("now").'.'.$format;

            $request->foto->storeAs('foto', $rename);
        } else {
            $rename = $dosen->foto;
        }

        $input = $request->all();
        $input['foto'] = $rename;

        $dosen->update($input);
        
        return redirect()->route('dosen.index')->with('success', 'Berhasil mengubah data dosen');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Dosen::findOrFail($id)->delete();
        return redirect()->route('dosen.index')->with('success', 'Berhasil menghapus data dosen');
    }
}
