<?php

namespace App\Http\Controllers;

use App\Almamater;
use App\Dosen;
use App\Pendidikan;
use App\Http\Requests\StorePendidikan;
use Illuminate\Http\Request;

class PendidikanController extends Controller
{
    public function __construct()
    {
        $this->middleware('kaprodi', ['only' => ['store', 'delete']]);
    }

    public function index($id)
    {
        $data['tab'] = 2;
        $data['dosen'] = Dosen::findOrFail($id);
        $data['pendidikan'] = $data['dosen']->pendidikan()->orderBy('tahun_lulus', 'desc')->get();
        return view('dosen.pendidikan.index', $data);
    }

    public function create($id)
    {
        $data['tab'] = 2;
        $data['dosen'] = Dosen::findOrFail($id);
        $data['almamater'] = Almamater::all();
        return view('dosen.pendidikan.create', $data);
    }

    public function store(StorePendidikan $request, $id)
    {
  
        //must have file upload, do rename and get extension of these files
        $format_ijazah = $request->ijazah->extension();
        $format_transkrip = $request->transkrip->extension();

        $rename_ijazah = 'ijazah_'.$id.'_'.strtotime("now").'.'.$format_ijazah;
        $rename_transkrip = 'transkrip_'.$id.'_'.strtotime("now").'.'.$format_transkrip;

        $request->file('ijazah')->storeAs('ijazah', $rename_ijazah);
        $request->file('transkrip')->storeAs('transkrip', $rename_transkrip);

        //insert to db 
        Pendidikan::create([
            'almamater_id' => $request->almamater_id,
            'jenjang_pendidikan' => $request->jenjang_pendidikan,
            'bidang_studi' => $request->bidang_studi,
            'gelar' => $request->gelar,
            'tahun_lulus' => $request->tahun_lulus,
            'ijazah' => $rename_ijazah,
            'transkrip' => $rename_transkrip,
            'dosen_id' => $id
        ]);

        return redirect()->route('pendidikan.index', $id)->with('success', 'Berhasil menambah pendidikan dosen');
    }

    public function edit($id)
    {
        $data['tab'] = 2;
        $data['almamater'] = Almamater::all();
        $data['pendidikan'] = Pendidikan::findOrFail($id);
        $data['dosen'] = $data['pendidikan']->dosen;
        return view('dosen.pendidikan.edit', $data);
    }

    public function update(StorePendidikan $request, $id) 
    {
        $pendidikan = Pendidikan::findOrFail($id);
        $name_ijazah = $pendidikan->ijazah;
        $name_transkrip = $pendidikan->transkrip;

        //remove old files
        if($request->hasFile('ijazah')){
            if(is_file('storage/ijazah/'.$pendidikan->ijazah)){
                unlink('storage/ijazah/'.$pendidikan->ijazah);
            }
            $format_ijazah = $request->ijazah->extension();
            $name_ijazah = 'ijazah_'.$pendidikan->dosen_id.'_'.strtotime("now").'.'.$format_ijazah;
            $request->ijazah->storeAs('ijazah', $name_ijazah);
        }
        if($request->hasFile('transkrip')){
            if(is_file('storage/transkrip/'.$pendidikan->transkrip)){
                unlink('storage/transkrip/'.$pendidikan->transkrip);
            }
            $format_transkrip = $request->transkrip->extension();
            $name_transkrip = 'transkrip_'.$pendidikan->dosen_id.'_'.strtotime("now").'.'.$format_transkrip;
            $request->transkrip->storeAs('transkrip', $name_transkrip);
        }

        $pendidikan->update([
            'bidang_studi' => $request->bidang_studi,
            'jenjang_pendidikan' => $request->jenjang_pendidikan,
            'gelar' => $request->gelar,
            'tahun_lulus' => $request->tahun_lulus,
            'ijazah' => $name_ijazah,
            'transkrip' => $name_transkrip,
            'dosen_id' => $pendidikan->dosen_id,
        ]);

        return redirect()->route('pendidikan.index', $pendidikan->dosen_id)->with('success', 'Berhasil mengubah pendidikan dosen');
    }

    public function delete($id_pendidikan){
        //remove file first then delete column on the table
        $pendidikan = Pendidikan::find($id_pendidikan);
        $file_ijazah = $pendidikan->ijazah;
        $file_transkrip = $pendidikan->transkrip;
        if(is_file('storage/ijazah/'.$file_ijazah)){
            unlink('storage/ijazah/'.$file_ijazah);            
        } 
        if(is_file('storage/transkrip/'.$file_transkrip)) {
            unlink('storage/transkrip/'.$file_transkrip);
        }
        
        $pendidikan->delete();

        return redirect()->route('pendidikan.index', $pendidikan->dosen_id)->with('success', 'Berhasil menghapus pendidikan dosen');


    }
}
