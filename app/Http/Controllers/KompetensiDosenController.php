<?php

namespace App\Http\Controllers;

use App\Dosen;
use App\Kompetensi;
use Illuminate\Http\Request;

class KompetensiDosenController extends Controller
{
    public function __construct()
    {
        $this->middleware('kaprodi', ['only' => ['store', 'delete']]);
    }

    public function index($id)
    {
        $data['dosen'] = Dosen::find($id);
        $data['kompetensi_dosen'] = $data['dosen']->kompetensiDosen;
        $data['data_kompetensi'] = Kompetensi::all();
        $data['tab'] = 3;
        //dd($data['kompetensi_dosen']);
        return view('dosen.kompetensi.kompetensi_dosen', $data);
    }

    public function store(Request $request, $id)
    {
        $this->validate($request, ['kompetensi_id' => 'required']);
        $dosen = Dosen::findOrFail($id);
        $dosen->kompetensiDosen()->attach($request->kompetensi_id, ['keterangan' => $request->keterangan]);
        return redirect()->back()->with('success', 'Berhasil menambah kompetensi dosen');
    }

    public function delete($dosen_id, $kompetensi_id) 
    {
        $dosen = Dosen::findOrFail($dosen_id);
        $dosen->kompetensiDosen()->detach($kompetensi_id);
        return redirect()->back()->with('success', 'Berhasil menghapus kompetensi dosen');
    }
}
