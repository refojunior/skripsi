<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sertifikasi;
use App\Dosen;

class SertifikasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($dosen_id)
    {
        $dosen = new Dosen();
        $data['dosen'] = $dosen->findOrFail($dosen_id);
        $data['sertifikasi'] = $data['dosen']->sertifikasi;
        
        $data['tab'] = 8;
        return view('dosen.sertifikasi.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($dosen_id)
    {
        $data['dosen'] = Dosen::findOrFail($dosen_id);
        $data['tab'] = 8;
        return view('dosen.sertifikasi.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $dosen_id)
    {
        $this->validate($request, [
            'nomor_sertifikasi' => 'required',
            'tahun_sertifikasi' => 'required',
            'file_sertifikasi' => 'required'
        ]);

        if($request->hasFile('file_sertifikasi')){
            $format = $request->file_sertifikasi->extension();
            $rename = 'sertifikasi_'.strtotime("now").'.'.$format;

            $request->file_sertifikasi->storeAs('sertifikasi', $rename);
        } else {
            $rename = null;
        }

        $input = $request->all();
        $input['file_sertifikasi'] = $rename;
        $input['dosen_id'] = $dosen_id;

        Sertifikasi::create($input);
        
        return redirect()->route('sertifikasi.index', $dosen_id)->with('success', 'Berhasil menambah data sertifikasi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($dosen_id, $id)
    {
        $data['dosen'] = Dosen::findOrFail($dosen_id);
        $data['sertifikasi'] = Sertifikasi::findOrFail($id);
        $data['tab'] = 8;
        return view('dosen.sertifikasi.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $dosen_id, $id)
    {
        $sertifikasi = Sertifikasi::findOrFail($id);

        $this->validate($request, [
            'nomor_sertifikasi' => 'required',
            'tahun_sertifikasi' => 'required',          
        ]);

        if($request->hasFile('file_sertifikasi')){
            //remove old file
            if(is_file('storage/sertifikasi/'.$sertifikasi->file_sertifikasi)){
                unlink('storage/sertifikasi/'.$sertifikasi->file_sertifikasi);
            }

            $format = $request->file_sertifikasi->extension();
            $rename = 'sertifikasi_'.strtotime("now").'.'.$format;

            $request->file_sertifikasi->storeAs('sertifikasi', $rename);
        } else {
            $rename = $sertifikasi->file_sertifikasi;
        }

        $input = $request->all();
        $input['file_sertifikasi'] = $rename;
        $input['dosen_id'] = $dosen_id;

        $sertifikasi->update($input);

        return redirect()->route('sertifikasi.index', $dosen_id)->with('success', 'Berhasil mengubah data sertifikasi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function indexAll(Request $request)
    {
        $sertifikasi = new Sertifikasi();

        $data['sertifikasi'] = $sertifikasi::paginate(15);

        if(isset($request->search)){
            $data['sertifikasi'] = $sertifikasi->advanceSearch($request->all());
        }

        return view('sertifikasi.index', $data);
    }
}
