<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Exports\DosenExport;
use App\Exports\DosenProdiExport;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    public function exportDosen()
    {
        return Excel::download(new DosenExport, 'dosen.xlsx');
    }

    public function exportDosenProdi($prodi_id)
    {
        return Excel::download(new DosenProdiExport($prodi_id), 'dosen_prodi.xlsx');
    }
}
