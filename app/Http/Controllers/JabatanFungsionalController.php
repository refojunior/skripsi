<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dosen;
use App\JabatanFungsional;

class JabatanFungsionalController extends Controller
{

    public function __construct()
    {
        $this->middleware('kaprodi', ['only' => ['create', 'destroy']]);
    }

    public function index(Request $request)
    {
        //default
        $jf = new JabatanFungsional();
        $data['jabatan_fungsional'] = $jf->orderBy('jabatan_fungsional', 'asc')->paginate(15);

        if(isset($request->search)){
            $data['jabatan_fungsional'] = $jf->advanceSearch($request->all());
        }

        return view('jabatan_fungsional.index', $data);
    }


    public function indexDosen($dosen_id)
    {
        $data['tab'] = 4;
        $data['dosen'] = Dosen::findOrFail($dosen_id);
        $data['jabatan_fungsional'] = $data['dosen']->jabatanFungsional;
        return view('jabatan_fungsional.index_dosen', $data);
    }

    public function create($dosen_id)
    {
        $data['dosen'] = Dosen::findOrFail($dosen_id);
        $data['jabatan_fungsional'] = getJabatanFungsional();
        return view('jabatan_fungsional.create', $data);        
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'dosen_id' => 'required',
            'nomor_sk' => 'required',
            'tanggal_mulai' => 'required',
            'jabatan_fungsional' => 'required',           
        ]);

        JabatanFungsional::create($request->all());

        return redirect()->route('jabatan-fungsional.indexDosen', $request->dosen_id)->with('success', 'Berhasil menambah jabatan fungsional dosen');
     
    }

    public function edit($jabatan_id) 
    {
        $data['jabatan_fungsional'] = JabatanFungsional::findOrFail($jabatan_id);
        return view('jabatan_fungsional.edit', $data);
    }

    public function update($jabatan_id, Request $request)
    {
        $this->validate($request, [
            'dosen_id' => 'required',
            'nomor_sk' => 'required',
            'tanggal_mulai' => 'required',
            'jabatan_fungsional' => 'required',
        ]);

        JabatanFungsional::findOrFail($jabatan_id)->update($request->all());

        return redirect()->route('jabatan-fungsional.indexDosen', $request->dosen_id)->with('success', 'Berhasil mengubah jabatan fungsional dosen');
    }

    public function delete($id)
    {
        JabatanFungsional::findOrFail($id)->delete();
        return redirect()->back()->with('success', 'Berhasil menghapus jabatan fungsional dosen');
    }
}
