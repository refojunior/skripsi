<?php

namespace App\Http\Controllers;

use App\BidangIlmu;
use App\Dosen;
use App\Prodi;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $data['cart_rumpun'] = $this->chartRumpun()->getData();
        $data['total_dosen'] = Dosen::all()->count();
        $data['chart_prodi'] = $this->chartProdi();

         
                        
        return view('dashboard', $data);
    }

    public function chartRumpun()
    {
        $rumpun = new BidangIlmu();
        $data_rumpun = $rumpun->has('dosen')->get();

       
    
        $rumpun_dosen = [];
        foreach($data_rumpun as $key => $rumpun){
            $rumpun_dosen[$rumpun->nama_bidang_ilmu] = $data_rumpun[$key]->dosen()->count();
        }
        arsort($rumpun_dosen);
        $rumpun_dosen = array_slice($rumpun_dosen, 0, 6);

        $jml_dosen = [];
        $nama_rumpun = [];
        foreach($rumpun_dosen as $key => $value){
            array_push($jml_dosen, $value);
            array_push($nama_rumpun, $key);
        }
        
        return response()->json([
            'jml_dosen' => $jml_dosen,
            'nama_rumpun' => $nama_rumpun
        ], 200);
    }

    public function chartProdi()
    {
        $prodi = new Prodi();

        $data_prodi = $prodi->limit(4)->get();
        $chart_prodi = [];

        for($i = 0; $i < count($data_prodi); $i++){
            array_push($chart_prodi, [
                'nama_prodi' => $data_prodi[$i]->nama_prodi,
                'jml_dosen' => $data_prodi[$i]->dosen()->count()
            ]);           
        }
        
       return json_encode($chart_prodi);

    }
}
