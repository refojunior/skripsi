<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BidangIlmu;
use App\Dosen;
use App\User;
use Illuminate\Support\Facades\Auth;

class BidangIlmuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct(){
        $this->middleware('kaprodi', ['only' => ['create', 'edit', 'destroy']]);
    }

    public function index()
    {
        if(isset($_GET['type'])){
            $bidang_ilmu = BidangIlmu::has('dosen')->get();           
        } else {
            $bidang_ilmu = BidangIlmu::all();
        }
        return view('bidang_ilmu.index')->with('bidang_ilmu', $bidang_ilmu);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {   
    //     return view ('bidang_ilmu.create');
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {
    //     $this->validate($request, [
    //         'nama_bidang_ilmu' => 'required|unique:rumpun_bidang_ilmu',
    //         'level' => 'required|numeric'
    //     ]);

    //     BidangIlmu::create($request->all());

    //     return redirect()->route('bidang-ilmu.index')->with('success', 'Berhasil menambah data rumpun bidang ilmu');
    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit($id)
    // {
    //     $data['bidang_ilmu'] = BidangIlmu::find($id);
    //     return view('bidang_ilmu.edit', $data);
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, $id)
    // {
    //     $this->validate($request, [
    //         'nama_bidang_ilmu' => 'required',
    //         'level' => 'required|numeric'
    //     ]);
    //     BidangIlmu::find($id)->update($request->all());
    //     return redirect()->route('bidang-ilmu.index')->with('success', 'Berhasil mengedit data rumpun bidang ilmu');
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     BidangIlmu::findOrFail($id)->delete();
    //     return redirect()->route('bidang-ilmu.index')->with('success', 'Berhasil menghapus data rumpun bidang ilmu');
    // }


    public function dosen($id)
    {
        $bidang_ilmu = BidangIlmu::findOrFail($id);
        $dosen = $bidang_ilmu->dosen()->orderBy('nama', 'asc')->get();
        
        return view('bidang_ilmu.dosen')->with(['bidang_ilmu' => $bidang_ilmu, 'dosen' => $dosen]);
    }


    public function getLevelRumpun(Request $request)
    {
        
        $rumpun = BidangIlmu::where('parent_code', $request->parent_code)->get();
        return response()->json($rumpun);
    }
}
