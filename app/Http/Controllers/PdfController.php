<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dosen;
use App\Prodi;
use PDF;

class PdfController extends Controller
{
    public function dosen()
    {
        $data['dosen'] = Dosen::orderBy('nama', 'asc')->get();
        $pdf = PDF::loadView('pdf.print_dosen', $data);
        return $pdf->stream('data_dosen.pdf');
        //return view('pdf.print_dosen', $data);
    }

    public function dosenProdi($id)
    {
        $prodi = Prodi::findOrFail($id);
        $dosen = $prodi->dosen()->orderBy('nama', 'asc')->get();
        $data['dosen'] = $dosen;
        $pdf = PDF::loadView('pdf.print_dosen_prodi', $data);
        return $pdf->stream('data_dosen_prodi.pdf');
        //return view('pdf.print_dosen', $data);
    }
}
