<?php

namespace App\Http\Controllers;

use App\Almamater;
use App\Pendidikan;
use Illuminate\Http\Request;

class AlmamaterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
     public function __construct(){
        $this->middleware('kaprodi', ['only' => ['create', 'edit', 'destroy']]);
    }
    
    public function index()
    {
        return view('almamater.index')->with('almamater', Almamater::orderBy('nama_almamater')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('almamater.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_almamater' => 'required|unique:almamater,nama_almamater',
            'alamat' => 'required'
        ]);

        Almamater::create($request->all());
        return redirect()->route('almamater.index')->with('success', 'Berhasil menambahkan data almamater');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('almamater.edit')->with('almamater', Almamater::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_almamater' => 'required',
            'alamat' => 'required'
        ]);


        Almamater::findOrFail($id)->update($request->all());
        return redirect()->route('almamater.index')->with('success', 'Berhasil mengedit data almamater');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Almamater::findOrFail($id)->delete();
        return redirect()->route('almamater.index')->with('success', 'Berhasil menghapus data almamater');
    }

    public function dosen($id)
    {
        $pendidikan = Pendidikan::where('almamater_id', $id)->orderBy('gelar', 'asc')->get();

        $almamater = Almamater::find($id);
        return view('almamater.dosen')->with(['pendidikan' => $pendidikan, 'almamater' => $almamater]);
    }

}
