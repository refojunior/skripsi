<?php

namespace App\Http\Controllers;

use App\Dosen;
use App\Diklat; 
use App\Http\Requests\StoreDiklat;
use Illuminate\Http\Request;

class DiklatController extends Controller
{

    public function __construct()
    {
        $this->middleware('kaprodi', ['only' => ['create', 'store', 'edit', 'update', 'delete']]);
    }

    public function indexDosen($dosen_id)
    {
        $data['dosen'] = Dosen::find($dosen_id);
        $data['diklat'] = $data['dosen']->diklat;
        $data['tab'] = 5;
   
        return view('diklat.index_dosen', $data);
    }

    public function create($dosen_id)
    {
        $diklat = new Diklat();
        $dosen = Dosen::find($dosen_id);
        $tingkat = $diklat->tingkat();
        
        return view('diklat.create')->with(['dosen' => $dosen, 'tingkat' => $tingkat]);
    }

    public function store(StoreDiklat $request)
    {
        
        if($request->hasFile('file_sertifikat')){
            $format = $request->file_sertifikat->extension();
            $rename = 'sertifikat_diklat_'.strtotime('now'). "." .$format;

            $request->file_sertifikat->storeAs('diklat', $rename);
        } else {
            $rename = null;
        }

        $input = $request->all();
        $input['file_sertifikat'] = $rename;
        
        Diklat::create($input);
    
        return redirect()->route('diklat.indexDosen', $request->dosen_id)->with('success', 'Berhasil menambah diklat dosen');;
        
    }

    public function  edit($id)
    {
        $diklat = new Diklat();
        $data['diklat'] = $diklat->findOrFail($id);
        $data['tingkat'] = $diklat->tingkat();
        return view ('diklat.edit', $data);
    }

    public function update(StoreDiklat $request, $id)
    {
        $diklat = Diklat::find($id);

        if($request->hasFile('file_sertifikat')){
            //remove old photos
            if(is_file('storage/diklat/'.$diklat->file_sertifikat)){
                unlink('storage/diklat/'.$diklat->file_sertifikat);
            }

            $format = $request->file_sertifikat->extension();
            $rename = 'sertifikat_diklat_'.strtotime("now").'.'.$format;

            $request->file_sertifikat->storeAs('diklat', $rename);
        } else {
            $rename = $diklat->file_sertifikat;
        }

        $input = $request->all();
        $input['file_sertifikat'] = $rename;

        $diklat->update($input);

        return redirect()->route('diklat.indexDosen', $request->dosen_id)->with('success', 'Berhasil mengubah data diklat dosen');   
    }

    public function destroy($id) 
    {
        Diklat::findOrFail($id)->delete();
        return redirect()->back()->with('success', 'Berhasil menghapus data diklat');
        
    }


    public function index(Request $request)
    {
        $diklat = new Diklat();
        $data['diklat'] = $diklat->paginate(15);
        if(isset($request->search)){
            $data['diklat'] = $diklat->advanceSearch($request->all());
        }
        return view('diklat.dosen', $data);
    }
}
