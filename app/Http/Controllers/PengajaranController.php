<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pengajaran;
use App\Dosen;

class PengajaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($dosen_id)
    {
        $dosen = new Dosen();
        $data['dosen'] = $dosen->find($dosen_id);
        $data['pengajaran'] = $data['dosen']->pengajaran;
        
        $data['tab'] = 7;
        return view('dosen.pengajaran.index', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($dosen_id)
    {
        $data['dosen'] = Dosen::findOrFail($dosen_id);
        $data['tab'] = 7;
        return view('dosen.pengajaran.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($dosen_id, Request $request)
    {
        $this->validate($request, [
            'kode_kelas' => 'required',
            'tahun_ajar' => 'required',
            'semester' => 'required',
        ]);

        $input = $request->all();
        $input['dosen_id'] = $dosen_id;

        Pengajaran::create($input);
        
        return redirect()->route('pengajaran.index', $dosen_id)->with('success', 'Berhasil menambah data pengajaran');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($dosen_id, $id)
    {
        $data['dosen'] = Dosen::findOrFail($dosen_id);
        $data['pengajaran'] = Pengajaran::findOrFail($id);
        $data['tab'] = 7;
        return view('dosen.pengajaran.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $dosen_id, $id)
    {
        $pengajaran = Pengajaran::findOrFail($id);
        $this->validate($request, [
            'kode_kelas' => 'required',
            'tahun_ajar' => 'required',
            'semester' => 'required',           
        ]);

        $input = $request->all();
        $input['dosen_id'] = $dosen_id;

        $pengajaran->update($input);

        return redirect()->route('pengajaran.index', $dosen_id)->with('success', 'Berhasil mengubah data pengajaran');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function indexAll(Request $request)
    {
        $pengajaran = new Pengajaran();

        $data['pengajaran'] = $pengajaran::paginate(15);

        if(isset($request->search)){
            $data['pengajaran'] = $pengajaran->advanceSearch($request->all());
        }

        return view('pengajaran.index', $data);
    }
}
