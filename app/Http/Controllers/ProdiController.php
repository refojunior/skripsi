<?php

namespace App\Http\Controllers;

use App\Prodi;
use Illuminate\Http\Request;

class ProdiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('kaprodi', ['only' => ['create', 'edit', 'destroy']]);
    }

    public function index()
    {
        return view('prodi.index')->with('prodi', Prodi::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('prodi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['nama_prodi' => 'required|unique:prodi', 'jenjang_pendidikan' => 'required']);
        Prodi::create($request->all());
        return redirect()->route('prodi.index')->with('success', 'Berhasil menambah data prodi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['prodi'] = Prodi::find($id);
        return view('prodi.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['nama_prodi' => 'required', 'jenjang_pendidikan' => 'required']);
        Prodi::find($id)->update($request->all());
        return redirect()->route('prodi.index')->with('success', 'Berhasil mengedit data prodi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Prodi::find($id)->delete();
        return redirect()->route('prodi.index')->with('success', 'Berhasil menghapus data prodi');
    }


    public function dosen($id)
    {
        $prodi = Prodi::findOrFail($id);
        $dosen = $prodi->dosen()->orderBy('nama', 'asc')->get();
        return view('prodi.dosen')->with(['prodi' => $prodi, 'dosen' => $dosen]);
    }
}
