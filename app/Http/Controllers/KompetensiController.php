<?php

namespace App\Http\Controllers;

use App\Kompetensi;
use Illuminate\Http\Request;

class KompetensiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('kompetensi.index')->with('kompetensi', Kompetensi::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kompetensi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['nama_kompetensi' => 'required|unique:kompetensi']);
        Kompetensi::create($request->all());
        return redirect()->route('kompetensi.index')->with('success', 'Berhasil menambah data kompetensi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['kompetensi'] = Kompetensi::findOrFail($id);
        return view('kompetensi.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['nama_kompetensi' => 'required']);
        Kompetensi::findOrFail($id)->update($request->all());
        return redirect()->route('kompetensi.index')->with('success', 'Berhasil mengedit kompetensi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Kompetensi::findOrFail($id)->delete();
        return redirect()->route('kompetensi.index')->with('success', 'Berhasil menghapus kompetensi');
    }

    public function dosen($id)
    {
        $kompetensi = Kompetensi::findOrFail($id);
        $dosen = $kompetensi->kompetensiDosen()->orderBy('nama', 'asc')->get();
        return view('kompetensi.dosen')->with(['kompetensi' => $kompetensi, 'dosen' => $dosen]);
    }
}
