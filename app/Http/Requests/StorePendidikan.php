<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePendidikan extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'POST':
            {
                return [
                    'almamater_id' => 'required',
                    'bidang_studi' => 'required',
                    'gelar' => 'required',
                    'tahun_lulus' => 'required|numeric',
                    'ijazah' => 'required',
                    'transkrip' => 'required',
                    'dosen_id' => 'required',         
                ];
            }

            case 'PUT':
            case 'PATCH':
            {
                return [
                    'almamater_id' => 'required',
                    'bidang_studi' => 'required',
                    'gelar' => 'required',
                    'tahun_lulus' => 'required|numeric',                   
                ];
            }

        }
    }
}
