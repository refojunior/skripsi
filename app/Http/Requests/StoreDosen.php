<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Dosen;

class StoreDosen extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $dosen = Dosen::find($this->dosen);
        switch($this->method()){
            case 'POST': 
            {
                return [
                    'nidn' => 'required|numeric|digits:10|unique:dosen,nidn',
                    'nik' => 'required|numeric|unique:dosen,nik',
                    'kewarganegaraan' => 'required',
                    'nama' => 'required',
                    'gelar_depan' => '',
                    'gelar_belakang' => '',
                    'alamat' => 'required',
                    'email' => 'required|unique:dosen,email',
                    'jenis_kelamin' => 'required',
                    'agama' => 'required',
                    'prodi_id' => 'required',
                    'bidang_ilmu_id' => 'required',
                    'tipe_dosen' => 'required',
                    'status' => 'required'                    
                ];
            }
            case 'PUT':
            case 'PATCH':
            {                
                
                return [
                    'nidn' => 'required|numeric|digits:10|unique:dosen,nidn,'. $dosen->id,
                    'nik' => 'required|numeric|unique:dosen,nik,'. $dosen->id,
                    'kewarganegaraan' => 'required',
                    'nama' => 'required',
                    'gelar_depan' => '',
                    'gelar_belakang' => '',
                    'alamat' => 'required',
                    'email' => 'required|unique:dosen,email,' . $dosen->id,
                    'jenis_kelamin' => 'required',
                    'agama' => 'required',
                    'prodi_id' => 'required',
                    'bidang_ilmu_id' => 'required',
                    'tipe_dosen' => 'required',
                    'status' => 'required',
                    
                ];
            }
        }
    }
}
