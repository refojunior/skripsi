<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreDiklat extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if($this->method() == 'POST'){
            return [
                'nama_diklat' => 'required',
                'penyelenggara' => 'required',
                'tingkat' => 'required',
                'nomor_sertifikat' => 'required',
                'tahun_penyelenggaraan' => 'required',
                'tanggal_mulai' => 'required',
                'tanggal_selesai' => 'required',
                'tempat' => 'required',
                'dosen_id' => 'required',
                'file_sertifikat' => 'required'
            ];
        } else if($this->method() == 'PUT' || $this->method() == 'PATCH') {
            //file_sertifikat not required in update
            return [
                'nama_diklat' => 'required',
                'penyelenggara' => 'required',
                'tingkat' => 'required',
                'nomor_sertifikat' => 'required',
                'tahun_penyelenggaraan' => 'required',
                'tanggal_mulai' => 'required',
                'tanggal_selesai' => 'required',
                'tempat' => 'required',
                'dosen_id' => 'required',             
            ];
        }

    }
}
