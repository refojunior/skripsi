<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Pengajaran extends Model
{
    protected $fillable = [
        'kode_kelas',
        'semester',
        'tahun_ajar',
        'matakuliah',
        'dosen_id'
    ];

    protected $table = 'pengajaran';

    public function dosen(){
        return $this->belongsTo('App\Dosen');
    }


    public function advanceSearch($request)
    {
        //$jf = new JabatanFungsional();
        $query = DB::table('pengajaran');
    
        //dd($request);

        if($request['name'] != null){
            $query->join('dosen', 'dosen.id', '=', 'pengajaran.dosen_id')->where('nama', 'like', '%'.$request['name'].'%');
        }

        if($request['kode_kelas'] != null){
            $query->where('kode_kelas', 'like', '%'.$request['kode_kelas'].'%');
        }
        if($request['matakuliah'] != null){
            $query->where('matakuliah', 'like', '%'.$request['matakuliah'].'%');
        }

        if($request['semester'] != null){
            $query->where('semester', 'like', '%'.$request['semester'].'%');
        }

        if($request['tahun_ajar'] != null){
            $query->where('tahun_ajar', 'like', '%'.$request['tahun_ajar'].'%');
        }


        if($request['name'] == null && $request['semester'] == null && $request['tahun_ajar'] == null && $request['kode_kelas'] == null && $request['matakuliah'] == null){
            $query->join('dosen', 'dosen.id', '=', 'pengajaran.dosen_id');
        }
           

        //dd($query->dump());

        return $query->get();        
    }
}
