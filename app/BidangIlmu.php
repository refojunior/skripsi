<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BidangIlmu extends Model
{
    protected $fillable = [
        'kode',
        'nama_bidang_ilmu',
        'level',
        'parent_code',
        'deskripsi',
    ];

    protected $table = 'rumpun_bidang_ilmu';

    public function dosen(){
        return $this->hasMany('App\Dosen');
    }
}
