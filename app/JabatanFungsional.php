<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class JabatanFungsional extends Model
{
    protected $fillable = [
        'jabatan_fungsional', 
        'nomor_sk',
        'tanggal_mulai',
        'dosen_id',
    ];

    protected $table = 'jabatan_fungsional';

    public function dosen(){
        return $this->belongsTo('App\Dosen');
    }

    public function advanceSearch($request)
    {
        //$jf = new JabatanFungsional();
        $query = DB::table('jabatan_fungsional');

        if($request['name'] != null){
            $query->join('dosen', 'dosen.id', '=', 'jabatan_fungsional.dosen_id')->where('nama', 'like', '%'.$request['name'].'%');
        }

        if($request['jabatan_fungsional'] != null){
            $query->where('jabatan_fungsional', 'like', '%'.$request['jabatan_fungsional'].'%');
        }

        if($request['nomor_sk'] != null){
            $query->where('nomor_sk', 'like', '%'.$request['nomor_sk'].'%');
        }


        if($request['tanggal_mulai'] != null){
            $query->where('tanggal_mulai', 'like', '%'.$request['tanggal_mulai'].'%');
        } 

        if($request['name'] == null && $request['jabatan_fungsional'] == null && $request['nomor_sk'] == null && $request['mata_kuliah'] == null && $request['tanggal_mulai'] == null){
            $query->join('dosen', 'dosen.id', '=', 'jabatan_fungsional.dosen_id');
        }
           

        //dd($query->dump());

        return $query->get();        
    }
}
