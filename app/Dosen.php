<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dosen extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'nidn', 
        'nik',
        'kewarganegaraan',
        'nama',
        'gelar_depan',
        'gelar_belakang',
        'alamat',
        'email',
        'jenis_kelamin',
        'agama',
        'tempat_lahir',
        'tanggal_lahir',
        'prodi_id',
        'bidang_ilmu_id',
        'tipe_dosen',
        'status',
        'foto',
        'keterangan',
        'golongan'
    ];

    protected $table = 'dosen';

    public function kompetensiDosen(){
        return $this->belongsToMany('App\Kompetensi', 'kompetensi_dosen', 'dosen_id', 'kompetensi_id')->withPivot('keterangan')->withTimestamps();
    }

    public function kompetensi(){
        return $this->belongsTo('App\Kompetensi');
    }

    public function bidangIlmu(){
        return $this->belongsTo('App\BidangIlmu');
    }

    public function prodi(){
        return $this->belongsTo('App\Prodi');
    }

    public function pendidikan(){
        return $this->hasMany('App\Pendidikan');
    }

    public function jabatanFungsional(){
        return $this->hasMany('App\JabatanFungsional');
    }

    public function diklat(){
        return $this->hasMany('App\Diklat');
    }

    public function penelitianPengabdian() {
        return $this->hasMany('App\PenelitianPengabdian');
    }

    public function pengajaran() {
        return $this->hasMany('App\Pengajaran');
    }

    public function sertifikasi() {
        return $this->hasMany('App\Sertifikasi');
    }

    public function user(){
        return $this->hasOne('App\User');
    }

    public function getKelamin(){
        if($this->jenis_kelamin == 'l') {
            return "Laki - Laki";
        } else {
            return "Perempuan";
        }
    }

    public function getStatus(){
        if($this->status == 'aktif') {
            return "<span class='badge badge-success'>".$this->status."</span>";
        } else {
            return "<span class='badge badge-danger'>".$this->status."</span>";
        }
    }

    public function namaGelar()
    {
        if($this->gelar_depan == null) {
            return $this->gelar_depan . ' ' . $this->nama . ', ' . $this->gelar_belakang;
        } else {
            return $this->gelar_depan . '. ' . $this->nama . ', ' . $this->gelar_belakang;
        }
    }
}
