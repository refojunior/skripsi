<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Almamater extends Model
{
    protected $fillable = [
        'nama_almamater',
        'alamat'
    ];

    protected $table = 'almamater';

    public function dosen()
    {
        return $this->belongsTo('App\Dosen');
    }
    

}
