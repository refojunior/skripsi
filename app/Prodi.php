<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prodi extends Model
{
    protected $fillable = [
        'nama_prodi',
        'jenjang_pendidikan',
        'status', 
    ];

    protected $table = 'prodi';

    public function dosen(){
        return $this->hasMany('App\Dosen');
    }
}
