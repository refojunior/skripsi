<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class PenelitianPengabdian extends Model
{
    protected $fillable = [
        'tipe', 
        'judul_kegiatan',
        'tahun_kegiatan',
        'lokasi_kegiatan',
        'lama_kegiatan',
        'skim',
        'dosen_id'
    ];

    protected $table = 'penelitian_pengabdian';

    public function dosen()
    {
        return $this->belongsTo('App\Dosen');
    }


    public function advanceSearch($request)
    {
        //$jf = new JabatanFungsional();
        $query = DB::table('penelitian_pengabdian');
    

        if($request['name'] != null){
            $query->join('dosen', 'dosen.id', '=', 'penelitian_pengabdian.dosen_id')->where('nama', 'like', '%'.$request['name'].'%');
        }

        if($request['tipe'] != null){
            $query->where('tipe', 'like', '%'.$request['tipe'].'%');
        }

        if($request['skim'] != null){
            $query->where('skim', 'like', '%'.$request['skim'].'%');
        }

        if($request['judul_kegiatan'] != null){
            $query->where('judul_kegiatan', 'like', '%'.$request['judul_kegiatan'].'%');
        }

        if($request['tahun_kegiatan'] != null){
            $query->where('tahun_kegiatan', 'like', '%'.$request['tahun_kegiatan'].'%');
        }

        if($request['lokasi_kegiatan'] != null){
            $query->where('lokasi_kegiatan', 'like', '%'.$request['lokasi_kegiatan'].'%');
        } 

        if($request['name'] == null && $request['skim'] == null && $request['judul_kegiatan'] == null && $request['tahun_kegiatan'] == null && $request['lokasi_kegiatan'] == null && $request['tipe'] == null){
            $query->join('dosen', 'dosen.id', '=', 'penelitian_pengabdian.dosen_id');
        }
           

        //dd($query->dump());

        return $query->get();        
    }

}
