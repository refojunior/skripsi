<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Sertifikasi extends Model
{
    protected $fillable = [
        'nama_sertifikasi',
        'nomor_sertifikasi',
        'tahun_sertifikasi',
        'nomor_peserta',
        'nomor_registrasi',
        'file_sertifikasi',
        'dosen_id'
    ];

    protected $table = 'sertifikasi';


    public function dosen()
    {
        return $this->belongsTo('App\Dosen');
    }

    public function advanceSearch($request)
    {
        //$jf = new JabatanFungsional();
        $query = DB::table('sertifikasi');
        //dd($request);

        if($request['name'] != null){
            $query->join('dosen', 'dosen.id', '=', 'sertifikasi.dosen_id')->where('gelar', 'like', '%'.$request['name'].'%');
        }

        if($request['nama_sertifikasi'] != null){
            $query->where('nama_sertifikasi', 'like', '%'.$request['nama_sertifikasi'].'%');
        }

        if($request['nomor_sertifikasi'] != null){
            $query->where('nomor_sertifikasi', 'like', '%'.$request['nomor_sertifikasi'].'%');
        }

        if($request['tahun_sertifikasi'] != null){
            $query->where('tahun_sertifikasi', 'like', '%'.$request['tahun_sertifikasi'].'%');
        }

        if($request['nomor_peserta'] != null){
            $query->where('nomor_peserta', 'like', '%'.$request['nomor_peserta'].'%');
        }

        if($request['nomor_registrasi'] != null){
            $query->where('nomor_registrasi', 'like', '%'.$request['nomor_registrasi'].'%');
        } 

        if($request['name'] == null && $request['nomor_sertifikasi'] == null && $request['tahun_sertifikasi'] == null && $request['nomor_peserta'] == null && $request['nomor_registrasi'] == null && $request['nama_sertifikasi'] == null){
            $query->join('dosen', 'dosen.id', '=', 'sertifikasi.dosen_id');
        }
           

        //dd($query->dump());

        return $query->get();        
    }
}
