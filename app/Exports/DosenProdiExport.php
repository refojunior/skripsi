<?php

namespace App\Exports;

use App\Dosen;
use App\Prodi;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;

class DosenProdiExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    private $prodi_id;

    public function __construct($prodi_id)
    {
        $this->prodi_id = $prodi_id;
    }

    public function collection()
    {
        $dosen = DB::table('dosen')->select('nidn', 'nama', 'nama_prodi')->join('prodi', 'dosen.prodi_id', '=', 'prodi.id')->where('prodi_id', $this->prodi_id)->get();
        return $dosen;
    }
}
