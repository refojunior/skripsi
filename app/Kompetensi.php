<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kompetensi extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'nama_kompetensi', 
        'deskripsi',
        'status'
    ];

    protected $table = 'kompetensi';

    public function kompetensiDosen(){
        return $this->belongsToMany('App\Dosen', 'kompetensi_dosen', 'kompetensi_id', 'dosen_id');
    }
}
