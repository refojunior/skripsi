<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Diklat extends Model
{
    protected $fillable = [
        'nama_diklat',
        'penyelenggara',
        'tingkat',
        'nomor_sertifikat',
        'tahun_penyelenggaraan',
        'tanggal_mulai',
        'tanggal_selesai',
        'tempat',
        'file_sertifikat',
        'dosen_id'
    ];

    protected $table = 'diklat';

    protected $tingkatText = [
            0 => 'lokal',
            1 => 'regional',
            2 => 'nasional',
            3 => 'internasional'
        ];
    

    public function dosen(){
        return $this->belongsTo('App\Dosen');
    }

    public function tingkat(){
        return $this->tingkatText;
    }

    public function getTingkat()
    {
        return $this->tingkatText[$this->tingkat];
    }

    public function advanceSearch($request)
    {
        
        $query = DB::table('diklat');

        if($request['name'] != null){
            $query->join('dosen', 'dosen.id', '=', 'diklat.dosen_id')->where('nama', 'like', '%'.$request['name'].'%');
        }

        if($request['nama_diklat'] != null){
            $query->where('nama_diklat', 'like', '%'.$request['nama_diklat'].'%');
        }

        if($request['penyelenggara'] != null){
            $query->where('penyelenggara', 'like', '%'.$request['penyelenggara'].'%');
        }

        if($request['tingkat'] != null){
            $query->where('tingkat', 'like', '%'.$request['tingkat'].'%');
        }

        if($request['tahun'] != null){
            $query->where('tahun_penyelenggaraan', 'like', '%'.$request['tahun'].'%');
        }

        if($request['nomor_sertifikat'] != null){
            $query->where('nomor_sertifikat', 'like', '%'.$request['nomor_sertifikat'].'%');
        }

        if($request['tempat'] != null){
            $query->where('tempat', 'like', '%'.$request['tempat'].'%');
        }

        if($request['name'] == null && $request['nama_diklat'] == null && $request['penyelenggara'] == null && $request['tingkat'] == null && $request['tahun'] == null && $request['nomor_sertifikat'] == null && $request['tempat'] == null){
            $query->join('dosen', 'dosen.id', '=', 'diklat.dosen_id');
        }
           

        //dd($query->dump());

        return $query->get();        
    }
}
