<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pendidikan extends Model
{
    protected $fillable = [
        'almamater_id',
        'bidang_studi',
        'gelar',
        'tahun_lulus',
        'ijazah',
        'transkrip',
        'dosen_id',
        'status',
        'jenjang_pendidikan'
    ];

    protected $table = 'pendidikan';


    public function dosen(){
        return $this->belongsTo('App\Dosen');
    }

    public function almamater(){
        return $this->belongsTo('App\Almamater');
    }
}
