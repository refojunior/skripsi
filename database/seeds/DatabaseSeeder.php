<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BidangIlmuSeeder::class); 
        $this->call(KompetensiSeeder::class);
        $this->call(AlmamaterSeeder::class);
        $this->call(DosenSeeder::class);
        $this->call(PendidikanSeeder::class);
        $this->call(KompetensiDosenSeeder::class);
        $this->call(JabatanFungsionalSeeder::class);
        $this->call(DiklatSeeder::class);
        $this->call(PenelitianPengabdianSeeder::class);
    }
}
