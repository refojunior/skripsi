<?php

use Illuminate\Database\Seeder;

class KompetensiDosenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 300; $i++){
            DB::table('kompetensi_dosen')->insert([
                'dosen_id' => App\Dosen::select('id')->orderByRaw("Rand()")->first()->id,
                'kompetensi_id' => App\Kompetensi::select('id')->orderByRaw("Rand()")->first()->id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }

        
    }
}
