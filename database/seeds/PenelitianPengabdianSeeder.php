<?php

use Illuminate\Database\Seeder;

class PenelitianPengabdianSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\PenelitianPengabdian::class, 50)->create();
    }
}
