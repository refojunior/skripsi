<?php

use Illuminate\Database\Seeder;

class PengajaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Pengajaran::class, 50)->create();
    }
}
