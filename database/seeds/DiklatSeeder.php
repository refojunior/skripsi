<?php

use Illuminate\Database\Seeder;

class DiklatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Diklat::class, 100)->create();
    }
}
