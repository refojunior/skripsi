<?php

use Illuminate\Database\Seeder;

class AlmamaterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Almamater::class, 20)->create();
    }
}
