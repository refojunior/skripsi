<?php

use Illuminate\Database\Seeder;

class BidangIlmuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\BidangIlmu::class, 15)->create();
    }
}
