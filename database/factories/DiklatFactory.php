<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Diklat;
use Faker\Generator as Faker;

$factory->define(Diklat::class, function (Faker $faker) {
    $start_date = $faker->date('Y-m-d', 'now');
    $end_date = $faker->date('Y-m-d', $start_date);
    return [
        'nama_diklat' => $faker->sentence(2),
        'penyelenggara' => $faker->streetName,
        'tingkat' => $faker->randomElement([0,1,2,3]),
        'nomor_sertifikat' => 'SK-'.strtotime($faker->date('Y-m-d', 'now')),
        'tahun_penyelenggaraan' => $faker->year('now'),
        'tanggal_mulai' => $end_date,
        'tanggal_selesai' => $start_date,
        'tempat' => $faker->city,
        'file_sertifikat' => null,
        'dosen_id' => function(){
            return App\Dosen::all()->random()->id;
        }
    ];
});
