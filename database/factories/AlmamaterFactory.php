<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Almamater;
use Faker\Generator as Faker;

$factory->define(Almamater::class, function (Faker $faker) {
    return [
        'nama_almamater' => 'University of '. $faker->state,
        'alamat' => $faker->address 
    ];
});
