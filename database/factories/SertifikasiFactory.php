<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Sertifikasi;
use Faker\Generator as Faker;

$factory->define(Sertifikasi::class, function (Faker $faker) {
    return [
        'nama_sertifikasi' => $faker->sentence(2),
        'nomor_sertifikasi' => 'SK-'.mt_rand(20000,90000),
        'tahun_sertifikasi' => $faker->randomElement(range(2000, 2019)),
        'file_sertifikasi' => 'default.file',
        'dosen_id' => function(){
            return App\Dosen::all()->random()->id;
        }
    ];
});
