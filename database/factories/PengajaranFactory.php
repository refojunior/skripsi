<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Pengajaran;
use Faker\Generator as Faker;

$factory->define(Pengajaran::class, function (Faker $faker) {
    return [
        'kode_ajar' => 'KD-'.mt_rand(20000,90000),
        'semester' => $faker->randomElement(['genap', 'ganjil']),
        'tahun_ajar' => $faker->randomElement(range(2002, 2019)),
        'file' => 'default.file',
        'dosen_id' => function(){
            return App\Dosen::all()->random()->id;
        }


    ];
});
