<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Kompetensi;
use Faker\Generator as Faker;

$factory->define(Kompetensi::class, function (Faker $faker) {
    return [
        'nama_kompetensi' => $faker->streetName,
        'deskripsi' => $faker->sentence(5),        
    ];
});
