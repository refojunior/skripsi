<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Dosen;
use Faker\Generator as Faker;

$factory->define(Dosen::class, function (Faker $faker) {
    $nama = $faker->name;
    return [
        'nidn' => $faker->unique()->creditCardNumber,
        'nik' => $faker->unique()->creditCardNumber,
        'kewarganegaraan' => 'Indonesia',
        'nama' => $nama,
        'nama_gelar' => $nama. ' S.Kom,. M.Kom',
        'alamat' => $faker->address,
        'email' => $faker->unique()->safeEmail,
        'jenis_kelamin' =>  $faker->randomElement(['l', 'p']),
        'agama' => $faker->randomElement(['islam', 'kristen', 'buddha', 'konghucu', 'hindu']),
        'tempat_lahir' => $faker->city,
        'tanggal_lahir' => $faker->date('Y-m-d', '2000-01-01'),
        'prodi_id' => function(){
            return App\Prodi::all()->random()->id;
        },
        'bidang_ilmu_id' => function(){
            return App\BidangIlmu::all()->random()->id;
        }
    ];
});
