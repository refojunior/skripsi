<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\PenelitianPengabdian;
use Faker\Generator as Faker;

$factory->define(PenelitianPengabdian::class, function (Faker $faker) {
    return [
        'tipe' => $faker->randomElement(['penelitian', 'pengabdian']),
        'judul_kegiatan' => $faker->sentence(3),
        'lokasi_kegiatan' => $faker->city,
        'lama_kegiatan' => 1,
        'tahun_kegiatan' => $faker->year('now'),
        'skim' => $faker->randomElement(getSkim()),
        'dosen_id' => function(){
            return App\Dosen::all()->random()->id;
        }
    ];
});
