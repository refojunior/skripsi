<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Pendidikan;
use Faker\Generator as Faker;

$factory->define(Pendidikan::class, function (Faker $faker) {
    return [
        'bidang_studi' => $faker->sentence(2),
        'gelar' => $faker->sentence(2),
        'tahun_lulus' => $faker->year('now'),
        'ijazah' => 'ijazah_2_1574247854.pdf',
        'transkrip' => 'transkrip_2_1574247854.pdf',
        'jenjang_pendidikan' => $faker->randomElement(getJenjangPendidikan()),
        'dosen_id' => function(){
            return App\Dosen::all()->random()->id;
        },
        'almamater_id' => function(){
            return App\Almamater::all()->random()->id;
        },
        'status' => 1
    ];
});
