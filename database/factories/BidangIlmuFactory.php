<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\BidangIlmu;
use Faker\Generator as Faker;

$factory->define(BidangIlmu::class, function (Faker $faker) {
    return [
        'nama_bidang_ilmu' => $faker->company,
        'deskripsi' => $faker->sentence(5)
    ];
});
