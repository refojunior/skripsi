<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\JabatanFungsional;
use Faker\Generator as Faker;

$factory->define(JabatanFungsional::class, function (Faker $faker) {
    return [
        'jabatan_fungsional' => $faker->randomElement(getJabatanFungsional()),
        'nomor_sk' => 'SK-'.strtotime($faker->date('Y-m-d', 'now')),
        'tanggal_mulai' => $faker->date('Y-m-d', 'now'),
        'mata_kuliah' => $faker->word,
        'dosen_id' => function(){
            return App\Dosen::all()->random()->id;
        }
    ];
});
