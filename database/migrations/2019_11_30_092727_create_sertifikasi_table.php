<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSertifikasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sertifikasi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_sertifikasi');
            $table->string('nomor_sertifikasi');
            $table->integer('tahun_sertifikasi');
            $table->string('nomor_peserta')->nullable();
            $table->string('nomor_registrasi')->nullable();
            $table->string('file_sertifikasi');
            $table->bigInteger('dosen_id')->unsigned();

            $table->foreign('dosen_id')->references('id')->on('dosen')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sertifikasi');
    }
}
