<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDosenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dosen', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nidn')->default(0);
            $table->string('nik');
            $table->string('kewarganegaraan');
            $table->string('nama');
            $table->string('nama_gelar')->nullable();
            $table->text('alamat');
            $table->string('email')->unique();
            $table->enum('jenis_kelamin', ['l', 'p']);
            $table->string('agama');
            $table->string('tempat_lahir');
            $table->date('tanggal_lahir');
            $table->bigInteger('prodi_id')->unsigned();
            $table->bigInteger('bidang_ilmu_id')->unsigned();

            $table->foreign('prodi_id')->references('id')->on('prodi')->onUpdate('cascade');
            $table->foreign('bidang_ilmu_id')->references('id')->on('rumpun_bidang_ilmu')->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dosen');
    }
}
