<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKompetensiDosenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kompetensi_dosen', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('dosen_id')->unsigned();
            $table->bigInteger('kompetensi_id')->unsigned();
            $table->string('keterangan')->nullable();

            $table->foreign('dosen_id')->references('id')->on('dosen');
            $table->foreign('kompetensi_id')->references('id')->on('kompetensi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kompetensi_dosen');
    }
}
