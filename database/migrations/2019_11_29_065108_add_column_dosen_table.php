<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnDosenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dosen', function(Blueprint $table){
            $table->string('gelar')->nullable();
            $table->enum('tipe_dosen', ['homebase', 'dpk', 'lb', 'pns']);
            $table->enum('status', ['aktif', 'tidak aktif'])->default('aktif');
            $table->enum('golongan', ['III/A', 'III/B', 'III/C', 'III/D', 'IV/A', 'IV/B', 'IV/C', 'IV/E'])->nullable();
            $table->string('foto')->nullable();
            $table->string('keterangan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
