<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTablePenelitianPengabdian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('penelitian_pengabdian', function(Blueprint $table){
            $table->dropColumn('dana_dari_dikti');
            $table->dropColumn('dana_dari_institusi_lain');
            $table->dropColumn('dana_dari_perguruan_tinggi');
            $table->string('skim');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
