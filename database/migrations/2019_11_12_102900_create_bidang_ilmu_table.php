<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBidangIlmuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rumpun_bidang_ilmu', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('kode');
            $table->string('nama_bidang_ilmu');
            $table->integer('level');
            $table->integer('parent_code')->nullable();
            $table->text('deskripsi')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bidang_ilmu');
    }
}
