<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengajaranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengajaran', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_ajar');
            $table->string('semester');
            $table->string('tahun_ajar');
            $table->string('file');
            $table->bigInteger('dosen_id')->unsigned();

            $table->foreign('dosen_id')->references('id')->on('dosen');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajaran');
    }
}
