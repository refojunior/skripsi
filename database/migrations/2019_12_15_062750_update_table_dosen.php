<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableDosen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dosen', function(Blueprint $table){
            $table->string('gelar_depan')->after('nama_gelar')->nullable();
            $table->dropColumn('nama_gelar');
            $table->dropColumn('gelar');
            $table->string('gelar_belakang')->after('gelar_depan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
