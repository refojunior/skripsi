<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenelitianPengabdianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penelitian_pengabdian', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('tipe', ['penelitian', 'pengabdian']);
            $table->string('judul_kegiatan');
            $table->string('tahun_kegiatan');
            $table->string('lokasi_kegiatan');
            $table->string('lama_kegiatan');
            $table->bigInteger('dana_dari_dikti')->default(0)->nullable();
            $table->bigInteger('dana_dari_perguruan_tinggi')->default(0)->nullable();
            $table->bigInteger('dana_dari_institusi_lain')->default(0)->nullable();
            $table->bigInteger('dosen_id')->unsigned();

            $table->foreign('dosen_id')->references('id')->on('dosen');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penelitian_pengabdian');
    }
}
