<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePendidikanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendidikan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('almamater');
            $table->string('bidang_studi');
            $table->string('gelar');
            $table->integer('tahun_lulus');
            $table->string('ijazah');
            $table->string('transkrip');
            $table->bigInteger('dosen_id')->unsigned();
            $table->enum('status', [0, 1])->default(1);

            $table->foreign('dosen_id')->references('id')->on('dosen')->onDelete('cascade');            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pendidikan');
    }
}
