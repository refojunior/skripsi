<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeAlmamaterInTablePendidikan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pendidikan', function(Blueprint $table)
        {
            $table->dropColumn('almamater');
        });

        Schema::table('pendidikan', function(Blueprint $table)
        {
            $table->bigInteger('almamater_id')->unsigned()->after('dosen_id');
            $table->foreign('almamater_id')->references('id')->on('almamater');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('table_pendidikan', function (Blueprint $table) {
            //
        });
    }
}
